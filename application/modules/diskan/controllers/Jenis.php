<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'diskan';
	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		$this->load->model('M_jenisikan', 'jenis_ikan');
		$this->load->model('M_jeniskegiatan', 'jenis_kegiatan');
		$this->load->model('M_jenisolahan', 'jenis_olahan');
		$this->load->model('M_jenisalat', 'jenis_alat');
	}
	public function index($page)
	{
		$data['page'] = 'jenis/' . $page;
		$this->load->view($this->template, $data);
	}

	public function TablesJenisIkan()
	{
		$list = $this->db->get('tb_jenis_ikan')->result();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();
			$budidaya = ($r->budidaya == 0 ? '' : 'Budidaya');
			$tangkapan = ($r->tangkapan == 0 ? '' : 'Tangkapan');
			$benih = ($r->benih == 0 ? '' : 'Benih');
			$row[] = $no;
			$row[] = $r->nama_ikan;
			$row[] = $budidaya . ' ' . $tangkapan . ' ' . $benih;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $r->id_jenis_ikan . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $r->id_jenis_ikan . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jenis_ikan->count_all(),
			"recordsFiltered" => $this->jenis_ikan->count_filtered(),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function TablesJenisAlat()
	{
		$list = $this->jenis_alat->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->nama_alat;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $r->id_data_alat_tangkap . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $r->id_data_alat_tangkap . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jenis_alat->count_all(),
			"recordsFiltered" => $this->jenis_alat->count_filtered(),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function TablesJenisKegiatan()
	{
		$list = $this->jenis_kegiatan->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->nama_kegiatan;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $r->id_data_kegiatan . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $r->id_data_kegiatan . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jenis_kegiatan->count_all(),
			"recordsFiltered" => $this->jenis_kegiatan->count_filtered(),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function TablesJenisOlahan()
	{
		$list = $this->jenis_olahan->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->nama_jenis;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $r->id_jenis_olahan . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $r->id_jenis_olahan . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->jenis_olahan->count_all(),
			"recordsFiltered" => $this->jenis_olahan->count_filtered(),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById($jenis)
	{
		switch ($jenis) {
			case 'ikan':
				$Data = $this->jenis_ikan->GetDataById();
				break;
			case 'kegiatan':
				$Data = $this->jenis_kegiatan->GetDataById();
				break;
			case 'olahan':
				$Data = $this->jenis_olahan->GetDataById();
				break;
			case 'alat':
				$Data = $this->jenis_alat->GetDataById();
				break;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		foreach ($this->input->post('jenis') as $key => $value) {
			$FormData[] = [
				'nama_jenis' => $value
			];
		}

		switch ($this->input->post('data_jenis')) {
			case 'ikan':
				$data = $this->input->post();
				if ($id == null) {
					$d = array();
					$row = array();
					$no = 0;
					foreach ($data as $key => $v) {
						if ($data['jenis'][$no] != 0 || $data['jenis'][$no] != '' || $data['jenis'][$no] != null) {
							$row['id_jenis_ikan'] = $id;
							$row['nama_ikan'] = $data['jenis'][$no];
							$row['budidaya'] = $data['budidaya'][$no];
							$row['tangkapan'] = $data['tangkapan'][$no];
							$row['benih'] = $data['benih'][$no];
							$d[] = $row;
						}
						$no++;
					}
					$result = $this->db->insert_batch('tb_jenis_ikan', $d);
				} else {

					$row = array();
					$row['id_jenis_ikan'] = $id;
					$row['nama_ikan'] = $data['jenis'][0];
					$row['budidaya'] = $data['budidaya'][0];
					$row['tangkapan'] = $data['tangkapan'][0];
					$row['benih'] = $data['benih'][0];

					$this->db->set($row);
					$this->db->where('id_jenis_ikan', $id);
					$result = $this->db->update('tb_jenis_ikan');
				}

				break;
			case 'kegiatan':
				foreach ($this->input->post('jenis') as $key => $value) {
					$Data[] = [
						'nama_kegiatan' => $value
					];
				}
				if ($id == null) {
					$result = $this->jenis_kegiatan->Created($Data);
				} else {
					foreach ($Data as $key => $v) {
						$Data[$key]['id_data_kegiatan'] = $id;
					}
					$result = $this->jenis_kegiatan->Updated($Data);
				}
				// $this->output->set_content_type('application/json')->set_output(json_encode($id));
				break;
			case 'olahan':
				if ($id == null) {
					$result = $this->jenis_olahan->Created($FormData);
				} else {
					foreach ($FormData as $key => $v) {
						$FormData[$key]['id_jenis_olahan'] = $id;
					}
					$result = $this->jenis_olahan->Updated($FormData);
				}
				$this->output->set_content_type('application/json')->set_output(json_encode($FormData));
				break;
			case 'alat':
				foreach ($this->input->post('jenis') as $key => $value) {
					$Alat[] = [
						'nama_alat' => $value
					];
				}
				if ($id == null) {
					$result = $this->jenis_alat->Created($Alat);
				} else {
					foreach ($Alat as $key => $v) {
						$Alat[$key]['id_data_alat_tangkap'] = $id;
					}
					$result = $this->jenis_alat->Updated($Alat);
				}
				break;
		}

		// $this->output->set_content_type('application/json')->set_output(json_encode(['data' => $FormData]));
	}

	public function Delete($jenis)
	{
		switch ($jenis) {
			case 'ikan':
				$result = $this->jenis_ikan->Deleted($this->input->post('id'));
				break;
			case 'kegiatan':
				$result = $this->jenis_kegiatan->Deleted($this->input->post('id'));
				break;
			case 'olahan':
				$result = $this->jenis_olahan->Deleted($this->input->post('id'));
				break;
			case 'alat':
				$result = $this->jenis_alat->Deleted($this->input->post('id'));

				break;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}

/* End of file Jenis.php */
/* Location: ./application/modules/diskan/controllers/Jenis.php */