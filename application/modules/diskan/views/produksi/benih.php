<style>
    .card {
        /* width: 1440px !important; */
        padding-left: 0px;
    }


</style>
<div class="container-fluid">
    <div class="card">
        <div class="col-lg-12 ">
            <div class="row p-4 align-items-center">
                <div class="col-lg-12">
                    <div class="button-group">
                        <button type="button" class="btn waves-effect waves-light btn-outline-success" id="trx">Transaksi</button>
                        <button type="button" class="btn waves-effect waves-light btn-outline-warning" id="upr">UPR Tersedia</button>
                    </div>
                </div>
            </div>
            <hr> 
            <div id="container">

            </div>
        </div>
    </div>

</div>

<!-- <div class="container-fluid" id="container">

</div> -->

<script type="text/javascript">
    const base_url = '<?= base_url() ?>';
    $(function() {
        $('#container').load(base_url + 'produksi-benih/load/trx');
        $('#trx').on('click', function() {
            $('#container').load(base_url + 'produksi-benih/load/trx');
        });
        $('#upr').on('click', function() {
            $('#container').load(base_url + 'produksi-benih/load/upr');
        })
    });
</script>