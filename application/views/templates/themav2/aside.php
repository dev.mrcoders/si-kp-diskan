<?php $mod = $this->access->modules(); ?>
<aside class="left-sidebar ">
  <div class="scroll-sidebar">
    <?php if ($this->session->userdata('data')->id_role != 'R3') : ?>
      <nav class="sidebar-nav ">
      <?php else : ?>
        <nav class="sidebar-nav d-none d-sm-block">
        <?php endif ?>
        <ul id="sidebarnav">
          <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Navigation</span></li>
          <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>dashboard" aria-expanded="false"><i class="mdi mdi-av-timer"></i><span class="hide-menu">Dashboard</span></a>
          </li>
          <?php if ($mod == 'dispan') : ?>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>data-produksi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Produksi</span></a>
            </li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>data-komoditi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Komoditi <?= $mod ?></span></a>
            </li>
          <?php endif ?>
          <?php if ($mod == 'diskan') : ?>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Produksi</span></a>
              <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Budidaya Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-benih" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Benih Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Olahan Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-tangkapan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Tangkapan Ikan</span></a></li>
              </ul>
            </li>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Jenis</span></a>
              <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-kegiatan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Kegiatan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Produk Olahan</span></a></li>
              </ul>
            </li>
          <?php else : ?>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Produksi</span></a>
              <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Budidaya Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-benih" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Benih Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Olahan Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>produksi-tangkapan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Tangkapan Ikan</span></a></li>
              </ul>
            </li>
            <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Jenis</span></a>
              <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Ikan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-kegiatan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Kegiatan</span></a></li>
                <li class="sidebar-item"><a href="<?= base_url() ?>jenis-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Produk Olahan</span></a></li>
              </ul>
            </li>
          <?php endif ?>
        </ul>
        </nav>
  </div>
</aside>
<?php if ($this->session->userdata('data')->id_role == 'R3') : ?>
  <nav class="navbar navbar-dark navbar-expand fixed-bottom d-md-none d-lg-none d-xl-none p-0" id="#navbar-example2" style="background-color: #1F2C3A;">
    <ul class="navbar-nav nav-justified w-100">
      <li class="nav-item">
        <a href="#opening" class="nav-link text-center">
          <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
          </svg>
          <span class="small d-block">Home</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="<?= base_url() ?>data" class="nav-link text-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-clipboard-data" viewBox="0 0 16 16">
            <path d="M4 11a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1zm6-4a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7zM7 9a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V9z" />
            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
            <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
          </svg>
          <span class="small d-block">Data</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="<?= base_url() ?>login/islogout" class="nav-link text-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z" />
            <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
          </svg>
          <span class="small d-block">Log Out</span>
        </a>
      </li>

    </ul>
  </nav>
<?php endif ?>