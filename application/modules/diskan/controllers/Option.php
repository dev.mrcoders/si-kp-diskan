<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Option extends MX_Controller
{

	public function kecamatan()
	{
		$this->db->like('nama_kecamatan', $this->input->get('search'), 'BOTH');
		$Data = $this->db->get('tb_kecamatan')->result();
		foreach ($Data as $key => $value) {
			if ($value->kd_kecamatan != 0) {
				$Res[] = [
					'id' => $value->kd_kecamatan,
					'text' => $value->nama_kecamatan
				];
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}

	public function kelurahan()
	{
		$this->db->where('kd_kecamatan', $this->input->get('id'));
		$Data = $this->db->get('tb_kelurahan')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->kd_kelurahan,
				'text' => $value->nama_kelurahan
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}

	public function jenisikan()
	{
		$this->db->like('nama_ikan', $this->input->get('search'));
		$Data = $this->db->get('tb_jenis_ikan')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_jenis_ikan,
				'text' => $value->nama_ikan
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
	public function jeniskegiatan()
	{
		$this->db->like('nama_kegiatan', $this->input->get('search'));
		$Data = $this->db->get('tb_data_kegiatan')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_data_kegiatan,
				'text' => $value->nama_kegiatan
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
	public function jenisolahan()
	{
		$this->db->like('nama_jenis', $this->input->get('search'));
		$Data = $this->db->get('tb_jenis_olahan')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_jenis_olahan,
				'text' => $value->nama_jenis,
				'seleceted' => true
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
	public function jenisalat()
	{
		$this->db->like('nama_jenis', $this->input->get('search'));
		$Data = $this->db->get('tb_jenis_alat')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_jenis_olahan,
				'text' => $value->nama_jenis
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}

	public function ikanByType($param)
	{
		$data = $this->db->get_where('tb_jenis_ikan', [$param => 1])->result();
		$Res = array();
		foreach ($data as $key => $value) {
			$Res[] .= '.' . $value->nama_ikan;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
}

/* End of file Option.php */
/* Location: ./application/modules/diskan/controllers/Option.php */