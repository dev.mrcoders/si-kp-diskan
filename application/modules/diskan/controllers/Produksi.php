<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produksi extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'diskan';

	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		$this->load->model('M_olahan', 'olahan');
		$this->user = $this->session->userdata('data');
	}
	public function index($page)
	{
		$data['page'] = 'produksi/' . $page;
		$this->load->view($this->template, $data);
	}

	public function DataTables()
	{
		$data = array();
		if ($this->input->post('bulan')) {
			$this->db->where('SUBSTRING(created,6,2)', $this->input->post('bulan'));
		}
		if ($this->input->post('tahun')) {
			$this->db->where('SUBSTRING(created,1,4)', $this->input->post('tahun'));
		}
		if ($this->input->post('kecamatan')) {
			$this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
		}
		$budidaya = $this->db->get('tb_data_budidaya')->result();
		foreach ($budidaya as $key) {
			$row = [];
			$namakec = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['jumlah'] = number_format($key->jumlah);
			$row['nama_kecamatan'] = $namakec->nama_kecamatan;
			$row['created'] = $key->created;
			$row['aksi'] = '<button class="btn btn-outline-warning edit" data-id=' . $key->id_data_budidaya . ' data-kecamatan="' . $namakec->nama_kecamatan . '" data-ket="edit"><i class="fas fa-edit"></i> Ubah</button><button class="btn btn-outline-danger delete ml-2" data-id=' . $key->id_data_budidaya . ' data-kecamatan="' . $namakec->nama_kecamatan . '"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>';
			$ikan = $this->db->get('tb_jenis_ikan')->result();
			$n = 1;
			$arrIkan = [];
			foreach ($ikan as $v) {
				$id_jenis = $v->id_jenis_ikan;
				if ($v->budidaya == 1) {
					$row2 = [];
					$row2['no'] = $n;
					$row2['nama_ikan'] = $v->nama_ikan;
					$prod = $this->db->get_where('tb_data_budidaya_transaksi', ['id_data_budidaya' => $key->id_data_budidaya, 'id_jenis_ikan' => $id_jenis])->row();
					$produksi = ($prod->jumlah == '' ? ' ' : number_format($prod->jumlah));
					$row2['jumlah'] = $produksi;
					$arrIkan[] = $row2;
				}
				$n++;
			}
			$row['produksi'] = $arrIkan;
			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($budidaya),
			"recordsFiltered" => count($budidaya),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById()
	{
		$id = $this->input->get('id_data_budidaya');
		$this->db->select('*');
		$this->db->from('tb_data_budidaya');
		$this->db->join('tb_kecamatan', 'tb_data_budidaya.kd_kecamatan = tb_kecamatan.kd_kecamatan');
		$this->db->where('id_data_budidaya', $id);
		$Data = $this->db->get()->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		$post = $this->input->post();

		$data1 = [
			'jumlah' => str_replace(',', '', $post['jumlah']),
		];
		if ($id == null) {
			$data1['kd_kecamatan'] = $post['kec'];
			$data1['created'] = $post['thn'] . '-' . $post['bln'];
			$this->db->insert('tb_data_budidaya', $data1);
			$id_data = $this->db->insert_id();
		} else {
			$this->db->where('id_data_budidaya', $id);
			$this->db->update('tb_data_budidaya', $data1);
			$id_data = $id;
		}
		$ikan = $this->db->get('tb_jenis_ikan')->result();
		$data = array();
		foreach ($ikan as $key) {
			$id_jenis = $key->id_jenis_ikan;
			$row = array();
			if ($key->budidaya == 1) {
				if ($post[$key->nama_ikan]) {
					$row['id_data_budidaya'] = $id_data;
					$row['id_data_budidaya_transaksi'] = $post['id_' . $key->nama_ikan];
					$row['id_jenis_ikan'] = $id_jenis;
					$row['jumlah'] = $post[$key->nama_ikan];
					$data[] = $row;
				}
			}
		}
		if ($data == null) {
			$data = 'Tidak ada Transaksi';
		} else {
			if ($id == null) {
				$this->db->insert_batch('tb_data_budidaya_transaksi', $data);
			} else {
				foreach ($data as $tes) {
					$this->db->where('id_data_budidaya_transaksi', $tes['id_data_budidaya_transaksi']);
					$db = $this->db->get('tb_data_budidaya_transaksi')->row();
					if ($db == null) {
						$this->db->insert('tb_data_budidaya_transaksi', $tes);
					} else {
						$this->db->set($tes);
						$this->db->where('id_data_budidaya_transaksi', $tes['id_data_budidaya_transaksi']);
						$this->db->update('tb_data_budidaya_transaksi');
					}
				}
				// 	// $this->db->update_batch('tb_data_tangkapan_transaksi', $data, 'id_data_tangkapan');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function Delete()
	{
		$table = array('tb_data_budidaya', 'tb_data_budidaya_transaksi');
		$this->db->where('id_data_budidaya', $this->input->post('id'));
		$result = $this->db->delete($table);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function DataTablesForm()
	{
		$id_budidaya = $this->input->post('id_data_budidaya');
		$ikan = $this->db->get('tb_jenis_ikan')->result();
		$no = 1;
		foreach ($ikan as $val) {
			$id_jenis = $val->id_jenis_ikan;
			if ($val->budidaya == 1) {
				$row = array();
				if ($id_budidaya) {
					$this->db->where('id_data_budidaya', $id_budidaya);
					$attr = '';
				} else {
					$attr = 'readonly';
				}
				$row[] = $no;
				$row[] = $val->nama_ikan;
				$prod = $this->db->get_where('tb_data_budidaya_transaksi', ['id_jenis_ikan' => $id_jenis])->row();
				$jumlah = ($prod->jumlah == '' ? ' ' : $prod->jumlah);
				$row[] = '<input type="number" step="1" min="0"  name="' . $val->nama_ikan . '" value="' . $jumlah . '" class="form-control number-separator hitung ' . $val->nama_ikan . '" ' . $attr . '/> <input type="hidden" name="id_' . $val->nama_ikan . '" value="' . $prod->id_data_budidaya_transaksi . '"/>';
				$no++;
				$data[] = $row;
			}
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($data),
			"recordsFiltered" => count($data),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function Check()
	{
		$this->db->where('created', $this->input->post('tahun') . '-' . $this->input->post('bulan'));
		$this->db->where('kd_kecamatan', $this->input->post('kec'));
		$IsDataExist = $this->db->get('tb_data_budidaya');
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_data_budidaya_transaksi', ['id_data_budidaya' => $IsDataExist->row()->id_data_budidaya]);
			if ($DataTrx->num_rows() > 0) {
				$Response = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Master dan Data Transaksi Ada, Silahkan Ganti Bulan, Tahun atau Kecamatan',
					'data' => $IsDataExist->row()->id_data_budidaya,
					'jumlah' => $IsDataExist->row()->jumlah,
				];
			} else {
				$Response = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Data Master Ada, Silahkan Melakukan Penambahan Data',
					'data' => $IsDataExist->row()->id_data_budidaya,
					'jumlah' => $IsDataExist->row()->jumlah,
				];
			}
		} else {
			$Response = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Data Belum Ada, Silahkan Tambah Data',
				'data' => $IsDataExist->row()->id_data_budidaya
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}
}

/* End of file Produksi.php */
/* Location: ./application/modules/diskan/controllers/Produksi.php */