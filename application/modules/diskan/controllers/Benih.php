<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Benih extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'diskan';

	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		// $this->load->model('M_benih', 'benih');
		$this->user = $this->session->userdata('data');
	}
	public function index($page = null)
	{
		$data['page'] = 'produksi/' . $page;
		$this->load->view($this->template, $data);
	}
	public function load($page)
	{
		$this->load->view('produksi/' . $page);
	}
	public function DataTables()
	{
		// $list = $this->pembenihan->get_datatables();
		$data = [];
		$no = 1;
		if ($this->input->post('bulan')) {
			$this->db->where('SUBSTRING(created,6,2)', $this->input->post('bulan'));
		}
		if ($this->input->post('tahun')) {
			$this->db->where('SUBSTRING(created,1,4)', $this->input->post('tahun'));
		}
		if ($this->input->post('kecamatan')) {
			$this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
		}
		$pembenihan = $this->db->get('tb_data_pembenihan')->result();
		foreach ($pembenihan as $key) {
			$row = [];
			$namakec = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['kd_kecamatan'] = $key->kd_kecamatan;
			$row['nama_kecamatan'] = $namakec->nama_kecamatan;
			$upr = $this->db->get_where('tb_upr_tersedia', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['upr_tersedia'] = number_format($upr->jumlah_upr);
			$row['created'] = $key->created;
			$row['aksi'] = '<button class="btn btn-outline-warning edit" data-id="' . $key->id_data_pembenihan . '" data-kd="' . $key->kd_kecamatan . '"data-kecamatan="' . $namakec->nama_kecamatan . '" data-ket="edit"><i class="fas fa-edit"></i> Ubah</button><button class="btn btn-outline-danger delete ml-2" data-id="' . $key->id_data_pembenihan . '" data-kecamatan="' . $namakec->nama_kecamatan . '"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>';
			$ikan = $this->db->get('tb_jenis_ikan')->result();
			$n = 1;
			$arrIkan = [];
			foreach ($ikan as $v) {
				if ($v->benih == 1) {
					$row2 = [];
					$row2['no'] = $n;
					$row2['nama_ikan'] = $v->nama_ikan;
					$benih = $this->db->get_where('tb_data_pembenihan_transaksi', ['id_jenis_ikan' => $v->id_jenis_ikan, 'id_data_pembenihan' => $key->id_data_pembenihan])->row();
					$row2['nama_ikan'] = $v->nama_ikan;
					$row2['upr_aktif'] = ($benih->upr_aktif == '' ? ' ' : $benih->upr_aktif);
					$row2['periode'] = ($benih->periode == '' ? ' ' : $benih->periode);
					$row2['jumlah_bibit'] = ($benih->jumlah_bibit == '' ? ' ' : number_format($benih->jumlah_bibit));
					$row2['kapasitas'] = ($benih->kapasitas_min == '' ? ' ' : number_format($benih->kapasitas_min)) . ' - ' . ($benih->kapasitas_max == '' ? ' ' : number_format($benih->kapasitas_max));
					$n++;
					$arrIkan[] = $row2;
				}
				$row['produksi'] = $arrIkan;
			}
			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($pembenihan),
			"recordsFiltered" => count($pembenihan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function DataTablesUpr()
	{
		// $list = $this->pembenihan->get_datatables();
		$kecamatan = $this->db->get('tb_kecamatan')->result();
		$no = 1;
		foreach ($kecamatan as $key) {
			if ($key->kd_kecamatan != 0) {
				$row = [];
				$row['no'] = $no;
				$row['nama_kecamatan'] = $key->nama_kecamatan;
				$upr = $this->db->get_where('tb_upr_tersedia', ['kd_kecamatan' => $key->kd_kecamatan])->row();
				$row['jumlah_upr'] = $upr->jumlah_upr;
				$row['aksi'] = '<button class="btn waves-effect waves-light btn-outline-warning edit" data-id="' . $upr->id_upr_tersedia . '" data-kd="' . $key->kd_kecamatan . '"><i class="fas fa-edit"></i> Ubah</button>';
				$data[] = $row;
				$no++;
			}
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($kecamatan),
			"recordsFiltered" => count($kecamatan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function DataById()
	{
		$id = $this->input->get('id_data_pembenihan');
		$this->db->select('*');
		$this->db->from('tb_data_pembenihan');
		$this->db->join('tb_kecamatan', 'tb_data_pembenihan.kd_kecamatan = tb_kecamatan.kd_kecamatan');
		$this->db->join('tb_upr_tersedia', 'tb_data_pembenihan.kd_kecamatan = tb_upr_tersedia.kd_kecamatan');
		$this->db->where('id_data_pembenihan', $id);
		$Data = $this->db->get()->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		// $fields = $this->tangkapan->ListColumnName();
		$post = $this->input->post();

		if ($id == null) {
			$data1['kd_kecamatan'] = $post['kec'];
			$data1['created'] = $post['thn'] . '-' . $post['bln'];
			$this->db->insert('tb_data_pembenihan', $data1);
			$id_data = $this->db->insert_id();
		} else {
			$id_data = $id;
		}

		$ikan = $this->db->get('tb_jenis_ikan')->result();
		$data = array();
		foreach ($ikan as $key => $value) {
			$row = array();
			$id_ikan = $value->id_jenis_ikan;
			if ($value->benih == 1) {
				if ($post['upr_aktif'][$key] != '' && $post['periode'][$key] != '' && $post['jumlah_bibit'][$key] != '' && $post['kapasitas_min'][$key] != '') {
					$row['id_data_pembenihan_transaksi'] = $post['id_data_pembenihan_transaksi'][$key];
					$row['id_data_pembenihan'] = $id_data;
					$row['id_jenis_ikan'] = $id_ikan;
					$row['upr_aktif'] = $post['upr_aktif'][$key];
					$row['periode'] = $post['periode'][$key];
					$row['jumlah_bibit'] = $post['jumlah_bibit'][$key];
					$row['kapasitas_min'] = $post['kapasitas_min'][$key];
					$max = ($post['kapasitas_max'][$key] == '' || $post['kapasitas_min'][$key] > $post['kapasitas_max'][$key] ? $post['kapasitas_min'][$key] : $post['kapasitas_max'][$key]);
					$row['kapasitas_max'] = $max;
					$data[] = $row;
				}
			}
		}
		if ($data == null) {
			$data = 'Tidak ada Transaksi';
		} else {
			if ($id == null) {
				$this->db->insert_batch('tb_data_pembenihan_transaksi', $data);
			} else {
				foreach ($data as $tes) {
					$this->db->where('id_data_pembenihan_transaksi', $tes['id_data_pembenihan_transaksi']);
					$db = $this->db->get('tb_data_pembenihan_transaksi')->row();
					if ($db == null) {
						$this->db->insert('tb_data_pembenihan_transaksi', $tes);
					} else {
						$this->db->set($tes);
						$this->db->where('id_data_pembenihan_transaksi', $tes['id_data_pembenihan_transaksi']);
						$this->db->update('tb_data_pembenihan_transaksi');
					}
				}
				// 	// $this->db->update_batch('tb_data_tangkapan_transaksi', $data, 'id_data_tangkapan');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function Delete($data = null)
	{
		switch ($data) {
			case '1':
				$this->db->where('id_data_pembenihan', $this->input->post('id'));
				$result = $this->db->delete('tb_data_pembenihan_transaksi');
				break;

			default:
				if ($this->input->post('id')) {
					$table = array('tb_data_pembenihan', 'tb_data_pembenihan_transaksi');
					$this->db->where('id_data_pembenihan', $this->input->post('id'));
					$result = $this->db->delete($table);
				}
				break;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function Check()
	{
		$upr = $this->db->get_where('tb_upr_tersedia', ['kd_kecamatan' => $this->input->post('kec')])->row()->jumlah_upr;
		$this->db->where('created', $this->input->post('tahun') . '-' . $this->input->post('bulan'));
		$this->db->where('kd_kecamatan', $this->input->post('kec'));
		$IsDataExist = $this->db->get('tb_data_pembenihan');
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_data_pembenihan_transaksi', ['id_data_pembenihan' => $IsDataExist->row()->id_data_pembenihan]);
			if ($DataTrx->num_rows() > 0) {
				$Response = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Master dan Data Transaksi Ada, Silahkan Ganti Bulan, Tahun dan Kecamatan',
					'data' => $IsDataExist->row()->id_data_pembenihan,
					'kec' => $this->input->post('kec'),
					'upr' => $upr,
				];
			} else {
				$Response = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Data Master Ada, Silahkan Melakukan Penambahan Data',
					'data' => $IsDataExist->row()->id_data_pembenihan,
					'kec' => $this->input->post('kec'),
					'upr' => $upr,
				];
			}
		} else {
			$Response = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Data Belum Ada, Silahkan Tambah Data',
				'data' => $IsDataExist->row()->id_data_pembenihan,
				'kec' => $this->input->post('kec'),
				'upr' => $upr,
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}
	public function DataTablesForm()
	{
		$id_pembenihan = $this->input->post('id_data_pembenihan');
		$kd_kecamatan = $this->input->post('kd_kecamatan');
		$ikan = $this->db->get('tb_jenis_ikan')->result();
		$no = 1;
		foreach ($ikan as $val) {
			$id = $val->id_jenis_ikan;
			if ($val->benih == 1) {
				$row = array();
				$tb = $this->db->get_where('tb_data_pembenihan_transaksi', ['id_data_pembenihan' => $id_pembenihan, 'id_jenis_ikan' => $id])->row();
				$row[] = '<input type="hidden" name="id_data_pembenihan_transaksi[]" value="' . $tb->id_data_pembenihan_transaksi . '" readonly class="form-control"/>';
				$row[] = $val->nama_ikan;
				$upr = $this->db->get_where('tb_upr_tersedia', ['kd_kecamatan' => $kd_kecamatan])->row();
				$row[] = '<input name="upr_aktif[]" type="number" min="0" max="' . $upr->jumlah_upr . '" step="1" class="form-control upr" value="' . $tb->upr_aktif . '"/>';
				$row[] = '<input name="periode[]" type="number" min="0"  step="1" class="form-control" value="' . $tb->periode . '"/>';
				$row[] = '<input name="jumlah_bibit[]" type="number" min="0"  step="1" class="form-control" value="' . $tb->jumlah_bibit . '"/>';
				$row[] = '<input name="kapasitas_min[]" type="number" min="0"  step="1" class="form-control min' . $no . '" value="' . $tb->kapasitas_min . '"/>';
				$row[] = '<input name="kapasitas_max[]" type="number" min="0"  step="1" class="form-control max' . $no . '" value="' . $tb->kapasitas_max . '"/>';
				$data[] = $row;
				$no++;
			}
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($ikan),
			"recordsFiltered" => count($ikan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataUpr()
	{
		$kecamatan = $this->input->post('kd_kecamatan');
		$tb = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $kecamatan])->row();
		$data = $this->db->get_where('tb_upr_tersedia', ['kd_kecamatan' => $kecamatan]);
		if ($data->num_rows() > 0) {
			$kdkec = $kecamatan;
			$kec = $tb->nama_kecamatan;
			$jml = $data->row()->jumlah_upr;
			$id = $data->id_upr_tersedia;
		} else {
			$kdkec = $kecamatan;
			$kec = $tb->nama_kecamatan;
			$jml = '';
			$id = '';
		}

		$this->output->set_content_type('application/json')->set_output(json_encode(['kd' => $kdkec, 'nama' => $kec, 'jml' => $jml, 'id' => $id]));
	}
	public function saveUPR()
	{
		$data = $this->input->post();
		if ($this->input->post('id_upr') != '') {
			$data = [
				'kd_kecamatan' => $this->input->post('kd'),
				'jumlah_upr' => $this->input->post('jml'),
				'id_upr_tersedia' => $this->input->post('id_upr'),
			];
			$this->db->set($data);
			$this->db->where('id_upr_tersedia', $this->input->post('id_upr'));
			$Res = $this->db->update('tb_upr_tersedia');
		} else {
			$data = [
				'kd_kecamatan' => $this->input->post('kd'),
				'jumlah_upr' => $this->input->post('jml'),
			];
			$this->db->insert('tb_upr_tersedia', $data);
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
}

/* End of file Produksi.php */
/* Location: ./application/modules/diskan/controllers/Produksi.php */