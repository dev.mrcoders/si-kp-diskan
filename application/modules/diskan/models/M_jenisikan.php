<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenisikan extends CI_Model {

	//set nama tabel yang akan kita tampilkan datanya
	var $table = 'tb_jenis_ikan';
    //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
	var $column_order = array(null, 'nama_ikan');

	var $column_search = array('nama_ikan');
    // default order 
	var $order = array('id_jenis_ikan' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->from($this->table);

		$i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                	$this->db->group_start();
                	$this->db->like($item, $this->input->post('search')['value']);
                } else {
                	$this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
        	$this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
    	$this->_get_datatables_query();
    	if ($this->input->post('length') != -1)
    		$this->db->limit($this->input->post('length'), $this->input->post('start'));
    	$query = $this->db->get();
    	return $query->result();
    }

    function count_filtered()
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all()
    {
    	$this->db->from($this->table);
    	return $this->db->count_all_results();
    }

    public function Created($data)
    {
    	return $this->db->insert_batch($this->table, $data);
    }

    public function Updated($data)
    {
    	return $this->db->update_batch($this->table, $data, 'id_jenis_ikan');
    }

    public function GetDataById()
    {
    	if ($this->input->get('id')) {
    		$this->db->where('id_jenis_ikan', $this->input->get('id'));
    		$hasil = $this->db->get($this->table)->row();
    	}else{
    		$hasil = $this->db->get($this->table)->result();
    	}

    	return $hasil;
    }

    public function Deleted($id)
    {
    	return $this->db->delete('tb_jenis_ikan', array('id_jenis_ikan' => $id));
    }

}

/* End of file M_jenisikan.php */
/* Location: ./application/modules/diskan/models/M_jenisikan.php */