<style>
    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

    table.dataTable thead th {
        white-space: nowrap
    }

    .select2-selection__rendered {
        line-height: 31px !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }

    table thead tr th {
        text-align: center !important;
    }

    #tb-data tbody td {
        text-align: center !important;
    }

    .modal {
        padding: 0 !important;
    }

    .modal .modal-dialog {
        width: 100%;
        max-width: none;
        height: 100%;
        margin: 0;
    }

    .modal .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0;
    }

    .modal .modal-body {
        max-height: calc(100vh - 150px);
        overflow-y: auto;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #586e73;
        border: 1px solid #fff;
        border-radius: 4px;
        cursor: default;
        float: left;
        margin-right: 5px;
        margin-top: 5px;
        padding: 0 5px;
    }

    .select2-container--classic .select2-selection--multiple .select2-selection__choice,
    .select2-container--default .select2-selection--multiple .select2-selection__choice,
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        background-color: #586e73;
        /* border-color: #137eff; */
        color: #fff;
    }
</style>

<div class="container-fluid">
    <div class="card">
        <div class="row p-4 align-items-center">
            <div class="col-sm-3">
                <label for="bulan">Bulan</label>
                <select name="bulan" id="" class="form-control select2 bulan"></select>
            </div>
            <div class="col-sm-3">
                <label for="bulan">Tahun</label>
                <select name="tahun" id="" class="form-control select2 tahun"></select>
            </div>
            <div class="col-sm-3">
                <label for="bulan">Kecamatan</label>
                <select name="kecamatan" id="" class="form-control select2 kecamatan" style="width: 100%;"></select>
            </div>
            <div class="col-sm-3">
                <div class="button-group pt-4" style="margin-left: 50px;">
                    <button type="button" id="filterData" class="btn waves-effect waves-light btn-outline-primary"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                    <button type="button" id="hapusFilter" class="btn waves-effect waves-light btn-outline-secondary"><i class="fa fa-times" aria-hidden="true"></i> Hapus Filter</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-body ">

                    <div class="row mb-4">
                        <div class="col-sm-3">
                            <h4 class="card-title">Data Produksi Olahan ikan</h4>

                        </div>

                        <div class="col-sm-9 text-right">
                            <button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-sm-2">
                            <input type="text" name="look" id="look" placeholder="Cari Data..." class="form-control">
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table nowrap" id="tb-data" width="100%">
                            <thead class="bg-info text-white">
                                <tr>
                                    <th></th>
                                    <th>Bulan Tahun</th>
                                    <th>Kecamatan</th>
                                    <th>Jumlah UPI</th>
                                    <th style="align-items: end; ">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal modal-fullscreen-xl" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Data Pengolahan Ikan</h4>
                <button type="button" class="close tutup" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="" id="form-olahan">
                <div class="modal-body" id="fields-komoditi">
                    <div class="row">
                        <table class="table " id="masterdata">
                            <tr class="bg-info text-light">
                                <td>Bulan</td>
                                <td>Tahun</td>
                                <td>Kecamatan</td>
                                <td>Jumlah UPI</td>
                            </tr>
                            <tr>
                                <td><select name="bln" id="bln" class="form-control select2"></select></td>
                                <td><select name="thn" id="thn" class="form-control select2"></select></td>
                                <td><select name="kec" id="kec" class="form-control select2" style="width: 100%;">

                                    </select></td>
                                <td><input type="number" step="1" min="0" id="jumlah_upi" name="jumlah_upi" class="form-control " placeholder="Jumlah UPI"></td>
                            </tr>
                        </table>
                    </div>
                    <p class="text-danger text-end">*Agar data dapat tersimpan, isi data satu baris tanpa ada yang terlewatkan</p>
                    <div class="row justify-content-center ">
                        <table class="table " id="tb-form-produksi">
                            <thead class="bg-info text-light ">
                                <tr>
                                    <th></th>
                                    <th>Jenis Kegiatan</th>
                                    <th>Nama Produk Olahan</th>
                                    <th>Volume Perbulan</th>
                                    <th>Siklus Produksi</th>
                                    <th>Produksi Tahunan</th>
                                </tr>
                            </thead>
                            <tbody id="td-bt">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn waves-effect waves-light btn-outline-danger tutup" data-dismiss="modal"> Tutup</button>
                    <button type="submit" class="btn waves-effect waves-light btn-outline-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    const base_url = '<?= base_url() ?>';
    let tags;
    let ops;

    $(function() {
        blnThn();
        DataTables();
        TablesForm();
        $('#hapusFilter').hide();
        var table;
        var tblen;
        var cek = 'add';
        var filter = [$(".bulan"), $(".tahun"), $(".kecamatan")];
        $("#look").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#tb-data tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        // Add event listener for opening and closing details
        $('#tb-data tbody').on('click', 'td.dt-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
        $('tbody').on('click', '.delete', function(event) {
            event.preventDefault();
            let id = $(this).data('id');
            let kecamatan = $(this).data('kecamatan');
            Swal.fire({
                title: 'Apakah Anda Yakin Menghapus Data Kecamatan ' + kecamatan,
                text: "Anda tidak akan dapat mengembalikan ini! Data Mana Yang ingin Anda Hapus?",
                icon: 'warning',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                denyButtonColor: '#ebba34',
                cancelButtonText: 'Batal',
                denyButtonText: 'Kosongkan Data',
                confirmButtonText: 'Hapus Data ',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url + 'produksi-olahan/delete/2',
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                            DataTables();
                            Swal.fire({
                                position: 'centered',
                                icon: 'success',
                                title: 'Berhasil Menghapus Data ' + kecamatan,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        },
                        error: function(request, textStatus, errorThrown) {
                            console.log(request.responseText);
                        }
                    });
                } else if (result.isDenied) {
                    $.ajax({
                        url: base_url + 'produksi-olahan/delete/1',
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                            console.log(data);
                            DataTables();
                            Swal.fire({
                                position: 'centered',
                                icon: 'success',
                                title: 'Berhasil Mengosongkan ' + kecamatan,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        },
                        error: function(request, textStatus, errorThrown) {
                            console.log(request.responseText);
                        }
                    });
                }
            })
        });
        $('.add').on('click', function(event) {
            tblen = $('#td-bt tr').length;
            event.preventDefault();
            $('#tb-form-produksi input').attr('readonly', true);
            $('#tb-form-produksi select').empty().trigger('change').attr('disabled', 'disabled');
            $('form#form-olahan input').val('');
            $('#add-modal').modal({
                backdrop: 'static',
                keyboard: false
            })
        });
        $('tbody').on('click', '.edit', function(event) {
            event.preventDefault();
            cek = 'edit';
            tblen = $('#tb-form-produksi tbody tr').length;
            let id = $(this).data('id');
            TablesForm(id);
            $('form#form-olahan').show();
            $('#add-modal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('form#form-olahan').attr('action', base_url + 'produksi-olahan/save/' + id);
            hitung();
            $.ajax({
                url: base_url + 'produksi-olahan/databyid',
                type: 'get',
                data: {
                    id_data_pengolahan: id
                },
                success: function(data) {
                    $('#tb-form-produksi tbody td input').attr("readonly", false);
                    $('#bln').val(data.created.substring(5)).prop('disabled', true);
                    $('#thn').val(data.created.substring(0, 4)).prop('disabled', true);
                    var OpsKec = new Option(data.nama_kecamatan, data.kd_kecamatan, true, true);

                    $('#kec').append(OpsKec).prop('disabled', true);
                    $('#jumlah_upi').val(data.jumlah_upi);
                }
            });

        });
        $('#kec').on('change', function() {
            check();
        });
        $('#filterData').on('click', function() {
            DataTables();
        });
        $('form#form-olahan').submit(function(e) {
            event.preventDefault();
            let FrmData = $('form#form-olahan').serialize();
            var formValidate = {
                Bulan: $('#bln').val(),
                Tahun: $('#thn').val(),
                Kecamatan: $('#kec').val(),
                Jumlah_UPI: $('#jumlah_upi').val(),
            }
            let stat = false;
            $.each(formValidate, function(index, value) {
                if (value == '') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Isi Data ' + index.replaceAll('_', ' ') + ' Terlebih Dahulu!',
                    })
                    return stat = false;
                } else {
                    return stat = true;
                }
            });
            if (stat == true) {
                $.ajax({
                    url: $('#form-olahan').attr('action'),
                    type: 'post',
                    data: FrmData,
                    success: function(data) {
                        $('#add-modal').modal('hide');
                        $('#kec').val(null).trigger('change');
                        DataTables();
                        Swal.fire({
                            position: 'centered',
                            icon: 'success',
                            title: 'Berhasil Mengunggah Data',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        console.log(data);

                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            }
        });
        $('#kec, .kecamatan').select2({
            // minimumInputLength: 1,
            allowClear: true,
            placeholder: 'masukkan nama kecamatan',
            ajax: {
                dataType: 'json',
                url: base_url + 'option/kecamatan',
                // delay: 200,
                data: function(params) {
                    return {
                        search: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $("#hapusFilter").on('click', function() {
            $.each(filter, function(index, value) {
                value.val(null).trigger('change');
            });
            $(this).hide();
            DataTables();
        });
        $.each(filter, function(index, value) {
            value.on('change', function() {
                $("#hapusFilter").show();
            });
        });
        $('#tb-form-produksi tbody').on('change', '#sel1', function() {
            console.log($(this).val());
        })


        function hitung() {
            for (let index = 1; index <= tblen; index++) {
                $('#td-bt').keyup('.create' + index, function() {
                    $('#td-bt .read' + index).val(parseInt($('#td-bt .create' + index).val()) * 12);
                });
            }
        }

        function myselect(elm) {
            $(elm).select2({
                width: "100%",
                allowClear: true,
                multiple: true,
                tags: true,
                tokenSeparators: [',', ' '],
                selectOnBlur: true,
                placeholder: 'masukkan nama produk',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'option/jenisolahan',
                    // delay: 200,
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                },
            });
        }

        function check() {
            $.ajax({
                url: base_url + 'produksi-olahan/check',
                type: 'post',
                data: {
                    bulan: $('#bln').val(),
                    tahun: $('#thn').val(),
                    kec: $('#kec').val(),
                },
                success: function(data) {
                    console.log(data.status);
                    let id = data.data;
                    if (data.status == 1) {
                        Swal.fire({
                            position: 'centered',
                            icon: 'success',
                            title: 'Data Ditemukan',
                            html: data.msg,
                            showConfirmButton: false,
                            timer: 2500
                        })
                        $('#tb-form-produksi tbody input').attr("readonly", true);
                    }
                    if (data.status == 2) {
                        Swal.fire({
                            position: 'centered',
                            icon: 'success',
                            title: 'Data Ditemukan',
                            html: data.msg,
                            showConfirmButton: false,
                            timer: 2500
                        })
                        $('#jumlah_upi').val(data.jumlah_upi);
                        TablesForm(data.data);
                        $('form#form-olahan').attr('action', base_url + 'produksi-olahan/save/' + id);
                        $('#tb-form-produksi tbody input').attr("readonly", false);
                        $('#tb-form-produksi tbody select').attr("disabled", false);
                        $('.reads').attr('readonly', true);
                        hitung();
                    }
                    if (data.status == 0) {
                        $('form#form-olahan input').val('');
                        $('form#form-olahan').attr('action', base_url + 'produksi-olahan/save/');
                        $('#tb-form-produksi tbody input').attr("readonly", false);
                        $('.reads').attr('readonly', true);
                        hitung();
                        $('#tb-form-produksi tbody select').attr("disabled", false);
                    }

                }
            });
        }

        function blnThn() {
            var ddlYears = $(".tahun");
            var thn = $("#thn");
            var thn1 = $(".tahun");
            var ddlMonth = $(".bulan");
            var bln1 = $("#bln");
            var currentYear = (new Date()).getFullYear();
            var currentMonth = new Date().getMonth();

            var totalMonths = 11;
            ddlYears.append('<option value="">-- Pilih Tahun --</option>');
            thn.append('<option value="">-- Pilih Tahun --</option>');
            for (var i = currentYear; i >= 2015; i--) {
                ddlYears.append('<option value="' + i + '">' + i + '</option>');
                thn.append('<option value="' + i + '">' + i + '</option>');
            }

            var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"];

            ddlMonth.append('<option value="">-- Pilih Bulan --</option>');
            bln1.append('<option value="">-- Pilih Bulan --</option>');
            for (var month = 0; month <= totalMonths; month++) {
                bln = ("0" + (month + 1)).slice(-2);
                ddlMonth.append('<option value="' + bln + '">' + monthNames[month] + '</option>');
                bln1.append('<option value="' + bln + '">' + monthNames[month] + '</option>');
            }

        }

        function DataTables() {
            let bulan = $('.bulan').val();
            let tahun = $('.tahun').val();
            let kecamatan = $('.kecamatan').val();
            table = $('#tb-data').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                stateSave: true,
                destroy: true,
                paging: false,
                "processing": false,
                "serverSide": false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false,
                "order": [
                    [1, 'asc']
                ],
                "ajax": {
                    "url": base_url + "produksi-olahan/tables",
                    "type": "POST",
                    "data": {
                        bulan: bulan,
                        tahun: tahun,
                        kecamatan: kecamatan,
                    }
                },
                "columns": [{
                        className: 'dt-control',
                        orderable: false,
                        data: null,
                        defaultContent: '',
                    },
                    {
                        data: 'created',
                    },
                    {
                        data: 'nama_kecamatan',
                    },
                    {
                        data: 'jumlah_upi',
                    },
                    {
                        data: 'aksi',
                    }
                ]

            });
        }

        function format(d) {
            // `d` is the original data object for the row
            let tables = '<table cellpadding="5" cellspacing="0" border="0" width="100%" style="padding-left:50px;"><thead class="bg-warning"><tr><th>NO</th><th>Jenis Kegiatan</th><th>Nama Produk Olahan</th><th>Volume Perbulan(Kg)</th><th>Siklus Produksi(hari/bulan)</th><th>Produksi Tahunan(Kg)</th></tr></thead><tbody>';
            $.each(d.produksi, function(index, value) {
                tables += '<tr >' +
                    '<td>' + value.no + '</td>' +
                    '<td>' + value.nama_kegiatan + '</td>' +
                    '<td>' + value.nama_produk_olahan + '</td>' +
                    '<td>' + value.volume_perbulan + '</td>' +
                    '<td>' + value.siklus_produksi + '</td>' +
                    '<td>' + value.produksi_tahunan + '</td>' +
                    '</tr>';
            });

            tables += '</tbody></table>';
            return tables;
        }

        function TablesForm(id = null) {
            let tables = $('#tb-form-produksi').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                stateSave: true,
                destroy: true,
                pageLength: 50,
                "dom": '<"dt-buttons"Bfli>rtp',
                "searching": false,
                "paging": false,
                "autoWidth": false,
                "fixedHeader": true,
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": base_url + "produksi-olahan/tables-form",
                    "type": "POST",
                    "data": function(data) {
                        data.id_data_pengolahan = id;
                    },
                    "dataSrc": function(response) {
                        // console.log(response);
                        ops = response.data;
                        return response.data;
                    }
                },
                drawCallback: function(set) {
                    for (let index = 1; index <= set.json['recordsTotal']; index++) {
                        idsel = '#sel' + String(index);
                        myselect(idsel);
                        $.each(set.json['data'][index - 1]['id_p'], function(index, value) {
                            var OpsAps = new Option(value, index, true, true);
                            $(idsel).append(OpsAps);
                        })
                    }
                }
            });
        }

    });
</script>