# SI-KP (Sistem Informasi Ketahanan Pangan)
## Dinas Pertanian

**Note: This project is still in progress, but welcome for any issues**

This repository is developed upon the following tools:

- [CodeIgniter](http://www.codeigniter.com/) (v3.1.13) - PHP framework
- [Bootstrap](http://getbootstrap.com/) (v4.1.x) - Popular frontend
- [Route Statics](https://github.com/Patroklo/codeigniter-static-laravel-routes) - library route statics
- [HMVC Module](https://github.com/N3Cr0N/CodeIgniter-HMVC) - Module create HMVC Codeigniter
- [NiceAdmin](https://www.wrappixel.com/templates/niceadmin/#demos) (v2.3.8) - bootstrap theme for Admin Panel


## Features

This repository contains setup for development
- Admin Panel withNiceAdmin Theme
- HMVC Konsep
- Route Statics Like Laravel
- Crud Data Produksi
- Crud Data jenis
- Change Profile Account
- Change Password Account

## Server Environment

Below configuration are preferred; other environments are not well-tested, but still feel free to report and issues.

- **PHP 7.4+ (PHP 8 not suported)**
- **Apache 2.2+**
- **MySQL 5.7+**

## Setup Guide

1. git clone this repo
2. Create a database(example: named "db_diskan"), then import /database/db_dispan.sql into MySQL server
3. Make sure the database config (/application/config/database.php) is set correctly


## Admin default login accounts

- Admin login (default username: Administrator password: 123456)
- password use MD5 Encrypt

