<?php $url = base_url().'assets/themav2/' ?>
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url('<?=base_url()?>assets/bg-1.jpeg') no-repeat center center;background-size: cover;">
    <div class="auth-box on-sidebar" style="position: relative;">
        <div id="loginform">
            <div class="logo">
                <span class="db"><img src="<?=$url?>logo_kampar.png" alt="logo" class="w-50" /></span>
                <h5 class="font-medium m-b-20">Sign In to Admin</h5>
            </div>

            <div class="row">
                <div class="col-12">
                    <form class="form-horizontal m-t-20" id="loginform" method="POST" action="<?=base_url()?>login-checking">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                            </div>
                            <input type="text" class="form-control form-control-lg" placeholder="Username" name="username" aria-label="username" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                            </div>
                            <input type="password" class="form-control form-control-lg" placeholder="Password" name="password" aria-label="Password" aria-describedby="basic-addon1">
                        </div>

                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

