<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	private $template = 'templates/themav2/index';
	public function index()
	{
		$this->db->where('kd_kecamatan !=', 0);
		$data['kecamatan'] = $this->db->get('tb_kecamatan')->num_rows();
		$data['kelurahan'] = $this->db->get('tb_kelurahan')->num_rows();
		$data['ikan'] = $this->db->get('tb_jenis_ikan')->num_rows();
		$data['page'] = 'dashboardv2';
		$this->load->view($this->template, $data);
	}
}

/* End of file Login.php */
/* Location: ./application/modules/Login/controllers/Login.php */