<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Olahan extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'diskan';

	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		$this->load->model('M_olahan', 'olahan');
		$this->user = $this->session->userdata('data');
	}
	public function index($page)
	{
		$data['page'] = 'produksi/' . $page;
		$this->load->view($this->template, $data);
	}

	public function DataTables()
	{
		$data = array();
		if ($this->input->post('bulan')) {
			$this->db->where('SUBSTRING(created,6,2)', $this->input->post('bulan'));
		}
		if ($this->input->post('tahun')) {
			$this->db->where('SUBSTRING(created,1,4)', $this->input->post('tahun'));
		}
		if ($this->input->post('kecamatan')) {
			$this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
		}
		$olahan = $this->db->get('tb_data_pengolahan')->result();
		foreach ($olahan  as $key) {
			$row = [];
			$namakec = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['jumlah_upi'] = number_format($key->jumlah_upi);
			$row['nama_kecamatan'] = $namakec->nama_kecamatan;
			$row['created'] = $key->created;
			$row['aksi'] = '<button class="btn btn-outline-warning edit" data-id=' . $key->id_data_pengolahan . ' data-kecamatan="' . $namakec->nama_kecamatan . '" data-ket="edit"><i class="fas fa-edit"></i> Ubah</button><button class="btn btn-outline-danger delete ml-2" data-id=' . $key->id_data_pengolahan . ' data-kecamatan="' . $namakec->nama_kecamatan . '"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>';
			$kegiatan = $this->db->get('tb_data_kegiatan')->result();
			$n = 1;
			$arrIkan = [];
			foreach ($kegiatan as $v) {
				$row2 = [];
				$namaproduk = '';
				$row2['no'] = $n;
				$row2['nama_kegiatan'] = $v->nama_kegiatan;
				$prod = $this->db->get_where('tb_data_pengolahan_transaksi', ['id_data_pengolahan' => $key->id_data_pengolahan, 'id_data_kegiatan' => $v->id_data_kegiatan]);
				foreach ($prod->result() as $r) {
					$p = $this->db->get_where('tb_jenis_olahan', ['id_jenis_olahan' => $r->nama_produk_olahan])->row();
					$namaproduk .= $p->nama_jenis . ', ';
				}
				$volume = ($prod->row()->volume_perbulan == '' ? ' ' : number_format($prod->row()->volume_perbulan));
				$siklus = ($prod->row()->siklus_produksi == '' ? ' ' : number_format($prod->row()->siklus_produksi));
				$produksi = ($prod->row()->produksi_tahunan == '' ? ' ' : number_format($prod->row()->produksi_tahunan));
				$row2['nama_produk_olahan'] =  (substr($namaproduk, 0, strlen($namaproduk) - 2) == false ? ' ' : substr($namaproduk, 0, strlen($namaproduk) - 2));
				$row2['volume_perbulan'] = $volume;
				$row2['siklus_produksi'] = $siklus;
				$row2['produksi_tahunan'] = $produksi;
				$arrIkan[] = $row2;
				// $namaproduk = ($prod->row()->nama_produk_olahan == '' ? ' ' : $prod->row()->nama_produk_olahan);
				$n++;
			}
			$row['produksi'] = $arrIkan;
			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($olahan),
			"recordsFiltered" => count($olahan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById()
	{
		$id = $this->input->get('id_data_pengolahan');
		$this->db->select('*');
		$this->db->from('tb_data_pengolahan');
		$this->db->join('tb_kecamatan', 'tb_data_pengolahan.kd_kecamatan = tb_kecamatan.kd_kecamatan');
		$this->db->where('id_data_pengolahan', $id);
		$Data = $this->db->get()->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		$post = $this->input->post();

		$data1 = [
			'jumlah_upi' => $post['jumlah_upi'],
		];
		if ($id == null) {
			$data1['kd_kecamatan'] = $post['kec'];
			$data1['created'] = $post['thn'] . '-' . $post['bln'];
			$this->db->insert('tb_data_pengolahan', $data1);
			$id_data = $this->db->insert_id();
		} else {
			$this->db->where('id_data_pengolahan', $id);
			$this->db->update('tb_data_pengolahan', $data1);
			$id_data = $id;
		}
		$kegiatan = $this->db->get('tb_data_kegiatan')->result();
		$data = array();
		$no = 0;
		foreach ($post['nama_produk_olahan'] as $row => $val) {
			foreach ($kegiatan as $key => $value) {
				$row = array();
				if ($post['nama_produk_olahan'][$no][$value->id_data_kegiatan] != '' && $post['volume_perbulan'][$key] != '' && $post['siklus_produksi'][$key]) {
					$row['id_data_pengolahan_transaksi'] = $post['id_data_transaksi'][$no];
					$row['id_data_pengolahan'] = $id_data;
					$row['id_data_kegiatan'] = $value->id_data_kegiatan;
					$row['nama_produk_olahan'] = $post['nama_produk_olahan'][$no][$value->id_data_kegiatan];
					$row['volume_perbulan'] = $post['volume_perbulan'][$key];
					$row['siklus_produksi'] = $post['siklus_produksi'][$key];
					$row['produksi_tahunan'] = $post['volume_perbulan'][$key] * 12;
					$data[] = $row;
				}
			};
			$no++;
		}
		if ($data == null) {
			$data = 'Tidak ada Transaksi';
		} else {
			if ($id == null) {
				$this->db->insert_batch('tb_data_pengolahan_transaksi', $data);
			} else {
				foreach ($data as $tes) {
					$this->db->where('id_data_pengolahan_transaksi', $tes['id_data_pengolahan_transaksi']);
					$db = $this->db->get('tb_data_pengolahan_transaksi')->row();
					if ($db == null) {
						$this->db->insert('tb_data_pengolahan_transaksi', $tes);
					} else {
						$this->db->set($tes);
						$this->db->where('id_data_pengolahan_transaksi', $tes['id_data_pengolahan_transaksi']);
						$this->db->update('tb_data_pengolahan_transaksi');
					}
				}
				// 	// $this->db->update_batch('tb_data_tangkapan_transaksi', $data, 'id_data_tangkapan');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function Delete($data = null)
	{
		switch ($data) {
			case '1':
				$this->db->where('id_data_pengolahan', $this->input->post('id'));
				$result = $this->db->delete('tb_data_pengolahan_transaksi');
				break;

			default:
				if ($this->input->post('id')) {
					$table = array('tb_data_pengolahan', 'tb_data_pengolahan_transaksi');
					$this->db->where('id_data_pengolahan', $this->input->post('id'));
					$result = $this->db->delete($table);
				}
				break;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function DataTablesForm()
	{
		$id_pengolahan = $this->input->post('id_data_pengolahan');
		$kegiatan = $this->db->get('tb_data_kegiatan')->result();
		$no = 1;
		foreach ($kegiatan as $val) {
			$id = $val->id_data_kegiatan;
			$row = array();
			if ($id_pengolahan) {
				$this->db->where('id_data_pengolahan', $id_pengolahan);
				$attr = '';
			} else {
				$attr = 'readonly';
			}
			$prod = $this->db->get_where('tb_data_pengolahan_transaksi', ['id_data_kegiatan' => $id]);
			$idtrx = '';
			$produk = array();
			foreach ($prod->result() as $r) {
				$p = $this->db->get_where('tb_jenis_olahan', ['id_jenis_olahan' => $r->nama_produk_olahan])->row();
				$produk[$p->id_jenis_olahan] = $p->nama_jenis;
				$idtrx .= '<input type="hidden" name="id_data_transaksi[]" value="' . $r->id_data_pengolahan_transaksi . '" readonly class="form-control"/>';
			}
			$row['id_p'] = $produk;
			// $namaolahan = $this->db->get_where('tb_jenis_olahan', ['id_jenis_olahan' => $prod->nama_produk_olahan])->row();
			$row[] = $idtrx;
			$row[] = $val->nama_kegiatan;
			$select = '<select class="form-control select2" name="nama_produk_olahan[][' . $val->id_data_kegiatan . ']" id="sel' . $no . '"> </select>';
			$row[] = $select;
			$volume = ($prod->row()->volume_perbulan == '' ? ' ' : $prod->row()->volume_perbulan);
			$row[] = '<input type="number" step="1" min="0"  name="volume_perbulan[]" value="' . $volume . '" class="form-control create' . $no . '" ' . $attr . '/> ';
			$siklus = ($prod->row()->siklus_produksi == '' ? ' ' : $prod->row()->siklus_produksi);
			$row[] = '<input type="number" step="1" min="0"  name="siklus_produksi[]" value="' . $siklus . '" class="form-control " ' . $attr . '/> ';
			$produksi = ($prod->row()->produksi_tahunan == '' ? ' ' : $prod->row()->produksi_tahunan);
			$row[] = '<input type="number" step="1" min="0"  name="produksi_tahunan[]" value="' . $produksi . '" class="form-control reads read' . $no . '" readonly/> ';
			$data[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($data),
			"recordsFiltered" => count($data),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function Check()
	{
		$this->db->where('created', $this->input->post('tahun') . '-' . $this->input->post('bulan'));
		$this->db->where('kd_kecamatan', $this->input->post('kec'));
		$IsDataExist = $this->db->get('tb_data_pengolahan');
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_data_pengolahan_transaksi', ['id_data_pengolahan' => $IsDataExist->row()->id_data_pengolahan]);
			if ($DataTrx->num_rows() > 0) {
				$Response = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Master dan Data Transaksi Ada, Silahkan Ganti bulan tahun dan Kecamatan',
					'data' => $IsDataExist->row()->id_data_pengolahan,
					'jumlah_upi' => $IsDataExist->row()->jumlah_upi,
				];
			} else {
				$Response = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Data Master Ada, Silahkan Melakukan Penambahan Data',
					'data' => $IsDataExist->row()->id_data_pengolahan,
					'jumlah_upi' => $IsDataExist->row()->jumlah_upi,
				];
			}
		} else {
			$Response = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Data Belum Ada, Silahkan Tambah Data',
				'data' => $IsDataExist->row()->id_data_tangkapan
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}
}

/* End of file Produksi.php */
/* Location: ./application/modules/diskan/controllers/Produksi.php */