<!-- Bootstrap tether Core JavaScript -->
<script src="<?= $url ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="<?= $url ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js" integrity="sha512-42PE0rd+wZ2hNXftlM78BSehIGzezNeQuzihiBCvUEB3CVxHvsShF86wBWwQORNxNINlBPuq7rG4WWhNiTVHFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js" integrity="sha512-LRkOtikKE2LFHPWiWh0/bfFynswxRwCZ5O7PkXTVFPcprw376xfOemiEHEOmCCmiwS6eLFUh2fb+Gqxc0waTSg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- apps -->
<script src="<?= $url ?>dist/js/app.min.js"></script>
<script src="<?= $url ?>dist/js/app.init.mini-sidebar.js"></script>
<script src="<?= $url ?>dist/js/app-style-switcher.js"></script>
<!-- <script src="<?= $url ?>dist/js/app-style-switcher.horizontal.js"></script> -->
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?= $url ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="<?= $url ?>assets/extra-libs/sparkline/sparkline.js"></script>
<!--Wave Effects -->
<script src="<?= $url ?>dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?= $url ?>dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="<?= $url ?>dist/js/custom.js"></script>
<!--This page JavaScript -->
<!--chartis chart-->
<!-- <script src="<?= $url ?>assets/libs/chartist/dist/chartist.min.js"></script> -->
<!-- <script src="<?= $url ?>assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script> -->
<!--c3 charts -->
<script src="<?= $url ?>assets/extra-libs/c3/d3.min.js"></script>
<script src="<?= $url ?>assets/extra-libs/c3/c3.min.js"></script>
<script src="<?= $url ?>assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?= $url ?>assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
<!-- <script src="<?= $url ?>dist/js/pages/dashboards/dashboard2.js"></script> -->
<!-- <script src="<?= $url ?>assets/extra-libs/DataTables/datatables.min.js"></script> -->

<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/rowgroup/1.3.0/js/dataTables.rowGroup.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>
<script src="<?= $url ?>form/isia-form-repeater.min.js"></script>
<script src="<?= $url ?>assets/libs/toastr/build/toastr.min.js"></script>

<script src="<?= $url ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="<?= $url ?>assets/libs/select2/dist/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="<?= $url ?>assets/libs/jquery.repeater/jquery.repeater.min.js"></script>
<script src="<?= $url ?>assets/js/easy-number-separator.js"></script>

<script>
  function Success(msg = '') {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: (msg != '' ? msg : 'Data Berhasil Disimpan'),
      showConfirmButton: false,
      timer: 1500
    })
  }
</script>