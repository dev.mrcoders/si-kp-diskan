<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_benih extends CI_Model {

       //set nama tabel yang akan kita tampilkan datanya
    var $table = array('tb_produksi_tangkapan','v_produksi_tangkapan');

    var $id_keys = 'id_produksi_tangkapan';
       //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order = array(null, 'nama_jenis');

    var $column_search = array('nama_jenis');
       // default order 
    var $order = array('id_produksi_tangkapan' => 'asc');

    public function __construct()
    {
      parent::__construct();
      $this->load->database();
  }

  private function _get_datatables_query()
  {
    $this->db->from($this->table[1]);

    $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table[1]);
        return $this->db->count_all_results();
    }

    public function Created($data)
    {
        return $this->db->insert_batch($this->table[0], $data);
    }

    public function Updated($data)
    {
        return $this->db->update_batch($this->table[0], $data, $this->id_keys);
    }

    public function GetDataById()
    {
        if ($this->input->get('id')) {
            $this->db->where($this->id_keys, $this->input->get('id'));
            $hasil = $this->db->get($this->table[1])->row();
        }else{
            $hasil = $this->db->get($this->table[0])->result();
        }

        return $hasil;
    }

    public function Deleted($id)
    {
        return $this->db->delete($this->table[0], array($this->id_keys => $id));
    }

    public function ListColumnName()
    {
        return $this->db->field_data($this->table[0]);
    }

}

/* End of file M_jenisikan.php */
/* Location: ./application/modules/diskan/models/M_jenisikan.php */