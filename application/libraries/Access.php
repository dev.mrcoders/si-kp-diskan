<?php
class Access
{
	protected $CI;
	function __construct($params=null)
	{
		$this->CI =& get_instance();
	}

	public function Modules($stat=false)
	{
		$hostname = parse_url(current_url());
		$subhost = explode('.',$hostname['host']);

		
		return $subhost[0];

	}

	public function Akses($module_name)
	{
		if($this->Modules() != $module_name){
			redirect(base_url().'errors','refresh');
		}
	}

	public function ListColumnName($table)
	{
		return $this->CI->db->field_data($table);
	}
}