<?php
Route::get('dashboard', 'dashboard');
Route::get('login/islogout', 'login/islogout');

Route::any('profile', 'Profile/profile/index', array(), function () {
	Route::get('(:any)', 'Profile/profile/index');
	Route::post('tables', 'Profile/profile/datatables');
	Route::post('save', 'Profile/profile/save');
	Route::post('updatepass', 'Profile/profile/updatepass');
});
// Routing untuk module dinas perikanan
Route::any('produksi-budidaya', 'diskan/produksi/index/budidaya', array(), function () {
	Route::post('tables', 'diskan/produksi/datatables');
	Route::post('tables-form', 'diskan/produksi/datatablesform');
	Route::post('check', 'diskan/produksi/check');
	Route::post('save', 'diskan/produksi/save');
	Route::post('save/(:num)', 'diskan/produksi/save/$1');
	Route::get('databyid', 'diskan/produksi/databyid');
	Route::post('delete', 'diskan/produksi/delete');
});
Route::any('produksi-olahan', 'diskan/olahan/index/olahan', array(), function () {
	Route::post('tables', 'diskan/olahan/datatables');
	Route::post('tables-form', 'diskan/olahan/datatablesform');
	Route::post('check', 'diskan/olahan/check');
	Route::post('save', 'diskan/olahan/save');
	Route::post('save/(:num)', 'diskan/olahan/save/$1');
	Route::get('databyid', 'diskan/olahan/databyid');
	Route::post('delete/(:num)', 'diskan/olahan/delete/$1');
});
Route::any('produksi-tangkapan', 'diskan/tangkapan/index/', array(), function () {
	Route::get('(:any)', 'diskan/tangkapan/index/$1');
	Route::post('tables', 'diskan/tangkapan/datatables');
	Route::post('tables-form', 'diskan/tangkapan/datatablesform');
	Route::post('check', 'diskan/tangkapan/check');
	Route::post('save', 'diskan/tangkapan/save');
	Route::post('save/(:num)', 'diskan/tangkapan/save/$1');
	Route::get('databyid', 'diskan/tangkapan/databyid');
	Route::post('delete/(:num)', 'diskan/tangkapan/delete/$1');
});
Route::any('produksi-benih', 'diskan/benih/index/benih', array(), function () {
	Route::get('(:any)', 'diskan/benih/index/$1');
	Route::post('tables', 'diskan/benih/datatables');
	Route::post('uprtables', 'diskan/benih/datatablesupr');
	Route::post('dataupr', 'diskan/benih/dataupr');
	Route::post('tables-form', 'diskan/benih/datatablesform');
	Route::post('check', 'diskan/benih/check');
	Route::post('save', 'diskan/benih/save');
	Route::post('saveupr', 'diskan/benih/saveupr');
	Route::post('save/(:num)', 'diskan/benih/save/$1');
	Route::get('databyid', 'diskan/benih/databyid');
	Route::post('delete/(:num)', 'diskan/benih/delete/$1');
	Route::get('load/(:any)', 'diskan/benih/load/$1');
});

Route::prefix('option', function () {
	Route::get('kecamatan', 'diskan/option/kecamatan');
	Route::get('kelurahan', 'diskan/option/kelurahan');
	Route::get('jenisikan', 'diskan/option/jenisikan');
	Route::get('jeniskegiatan', 'diskan/option/jeniskegiatan');
	Route::get('jenisolahan', 'diskan/option/jenisolahan');
	Route::get('jenisalat', 'diskan/option/jenisalat');
	Route::get('ikanbytype/(:any)', 'diskan/option/ikanbytype/$1');
});
