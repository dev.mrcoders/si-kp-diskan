<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i>
            </a>
            <div class="navbar-brand">
                <a href="<?= base_url()?>" class="logo">
                    <b class="logo-icon">
                        <img src="<?=$url?>logo_kampar.png" alt="homepage" class="dark-logo w-10" />
                        <img src="<?=$url?>logo_kampar.png" alt="homepage" class="light-logo w-15" />
                    </b>
                    <span class="logo-text font-weight-bold text-white">
                        DISKAN
                    </span>
                </a>
                <a class="sidebartoggler d-none d-md-block" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                    <i class="mdi mdi-toggle-switch mdi-toggle-switch-off font-20"></i>
                </a>
            </div>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav float-left mr-auto"></ul>
            <ul class="navbar-nav float-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?=$url?>assets/images/users/2.jpg" alt="user" class="rounded-circle" width="40">
                        <span class="m-l-5 font-medium d-none d-sm-inline-block">
                            <?=$this->session->userdata('data')->nip?> <i class="mdi mdi-chevron-down"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <span class="with-arrow">
                            <span class="bg-primary"></span>
                        </span>
                        <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                            <div class="">
                                <img src="<?=$url?>assets/images/users/2.jpg" alt="user" class="rounded-circle" width="60">
                            </div>
                            <div class="m-l-10">
                                <h4 class="m-b-0"><?=$this->session->userdata('data')->nama?></h4>
                                <p class=" m-b-0"><?=$this->session->userdata('data')->jabatan?></p>
                            </div>
                        </div>
                        <a class="dropdown-item" href="<?=base_url()?>profile">
                            <i class="ti-user m-r-5 m-l-5"></i> Profil
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?=base_url()?>login/islogout">
                            <i class="fa fa-power-off m-r-5 m-l-5"></i> Keluar
                        </a>
                        <div class="dropdown-divider"></div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>