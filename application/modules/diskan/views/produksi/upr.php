<style>
    table thead tr th {
        text-align: center !important;
    }
</style>
<div class="row">

    <div class="col-lg-12">
        <div class="card">

            <div class="card-body ">

                <div class="row mb-4">
                    <div class="col-sm-3">
                        <h4 class="card-title">Data UPR Tersedia</h4>

                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-sm-2">
                        <input type="text" name="look" id="look" placeholder="Cari Data..." class="form-control">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table nowrap" id="tb-data" width="100%">
                        <thead class="bg-info text-white">
                            <tr>
                                <th>No</th>
                                <th>Kecamatan</th>
                                <th>UPR Tersedia</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="add-modal" class="modal modal-fullscreen-xl" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Unit Pembenihan Raya (UPR)</h4>
                <button type="button" class="close tutup" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="" id="form-upr">
                <div class="modal-body" id="fields-komoditi">
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="Jumlah Upr">Jumlah UPR Tersedia</label>
                            <input type="number" class="form-control" name="jumlah_upr" id="jumlah_upr">
                            <input type="hidden" name="kd" id="kd">
                            <input type="hidden" name="id_upr" id="id_upr">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn waves-effect waves-light btn-outline-danger tutup" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn waves-effect waves-light btn-outline-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        DataTables();
        $("#look").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#tb-data tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('tbody').on('click', '.edit', function(event) {
            event.preventDefault();
            cek = 'edit';
            let id = $(this).data('id');
            let kd = $(this).data('kd');
            $('#id_upr').val(id);
            $('form#form-upr').show();
            $('#kd').val(kd);
            $('#add-modal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $.ajax({
                url: base_url + 'produksi-benih/dataupr',
                type: 'post',
                data: {
                    id_data_upr: id,
                    kd_kecamatan: kd,
                },
                success: function(data) {
                    $('#jumlah_upr').val(data.jml);
                }
            });

        });

        $('.kec').select2({
            // minimumInputLength: 1,
            allowClear: true,
            placeholder: 'masukkan nama kecamatan',
            ajax: {
                dataType: 'json',
                url: base_url + 'option/kecamatan',
                // delay: 200,
                data: function(params) {
                    return {
                        search: params.term
                    }
                },
                processResults: function(data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        function DataTables() {
            let bulan = $('.bulan').val();
            let tahun = $('.tahun').val();
            let kecamatan = $('.kecamatan').val();
            table = $('#tb-data').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                stateSave: true,
                destroy: true,
                paging: false,
                "processing": false,
                "serverSide": false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false,
                order: [
                    [0, 'asc']
                ],
                "ajax": {
                    "url": base_url + "produksi-benih/uprtables",
                    "type": "POST",
                    "data": {
                        bulan: bulan,
                        tahun: tahun,
                        kecamatan: kecamatan,
                    }
                },
                "columns": [{
                        data: "no",
                    },
                    {
                        data: 'nama_kecamatan',
                    },
                    {
                        data: 'jumlah_upr',
                    },
                    {
                        data: 'aksi',
                    }
                ],
                "columnDefs": [{
                        'targets': 0,
                        "className": "text-center",
                    },
                    {
                        'targets': 2,
                        "className": "text-center",
                    },
                    {
                        'targets': 3,
                        "className": "text-center",
                    }
                ]

            });
        }

        $('form#form-upr').submit(function(e) {
            event.preventDefault();

            $.ajax({
                url: base_url + 'produksi-benih/saveupr',
                type: 'post',
                data: {
                    kd: $('#kd').val(),
                    jml: $('#jumlah_upr').val(),
                    id_upr: $('#id_upr').val(),
                },
                success: function(data) {
                    DataTables();
                    $('#add-modal').modal('hide');
                    Swal.fire({
                        position: 'centered',
                        icon: 'success',
                        title: 'Berhasil Mengunggah Data',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    console.log(data);

                },
                error: function(request, textStatus, errorThrown) {
                    console.log(request.responseText);
                }
            });
        });
    });
</script>