<?php

use phpDocumentor\Reflection\Types\This;

defined('BASEPATH') or exit('No direct script access allowed');

class Tangkapan extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'diskan';

	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		$this->load->model('M_tangkapan', 'tangkapan');
		$this->user = $this->session->userdata('data');
	}
	public function index($page = null)
	{
		$data['alat'] = $this->db->get('tb_data_alat_tangkap')->result();
		$data['page'] = 'produksi/tangkapan';
		$this->load->view($this->template, $data);
	}

	public function DataTables()
	{
		// $list = $this->tangkapan->get_datatables();
		$data = [];
		$no = 1;
		if ($this->input->post('bulan')) {
			$this->db->where('SUBSTRING(created,6,2)', $this->input->post('bulan'));
		}
		if ($this->input->post('tahun')) {
			$this->db->where('SUBSTRING(created,1,4)', $this->input->post('tahun'));
		}
		if ($this->input->post('kecamatan')) {
			$this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
		}
		$tangkapan = $this->db->get('tb_data_tangkapan')->result();
		foreach ($tangkapan as $key) {
			$row = [];
			$namakec = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['kd_kecamatan'] = $key->kd_kecamatan;
			$row['jml_nelayan'] = $key->jml_nelayan;
			$row['perahu_motor_tempel'] = $key->perahu_motor_tempel;
			$row['perahu_tanpa_motor'] = $key->perahu_tanpa_motor;
			$row['nelayan_tanpa_perahu'] = $key->nelayan_tanpa_perahu;
			$row['nama_kecamatan'] = $namakec->nama_kecamatan;
			$row['jml_perahu'] = $key->perahu_motor_tempel + $key->perahu_tanpa_motor;
			$row['created'] = $key->created;
			$row['aksi'] = '<button class="btn btn-outline-warning edit" data-id=' . $key->id_data_tangkapan . ' data-kecamatan="' . $namakec->nama_kecamatan . '" data-ket="edit"><i class="fas fa-edit"></i> Ubah</button><button class="btn btn-outline-danger delete ml-2" data-id=' . $key->id_data_tangkapan . ' data-kecamatan="' . $namakec->nama_kecamatan . '"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>';
			$alat = $this->db->get('tb_data_alat_tangkap')->result();
			$n = 1;
			$arrIkan = [];
			foreach ($alat as $v) {
				$row2 = [];
				$row3 = [];
				$row2['no'] = $n;
				$row2['nama_alat'] = $v->nama_alat;
				$ikan = $this->db->get('tb_jenis_ikan')->result();
				foreach ($ikan as $val) {
					$id = $val->id_jenis_ikan;
					if ($val->tangkapan == 1) {
						$prod = $this->db->get_where('tb_data_tangkapan_transaksi', ['id_data_tangkapan' => $key->id_data_tangkapan, 'id_alat_tangkap' => $v->id_data_alat_tangkap, 'id_jenis_ikan' => $val->id_jenis_ikan])->row();
						$hasil = ($prod->produksi == '' ? ' ' : $prod->produksi);
						$row2[$val->nama_ikan] = $hasil;
						$row3[] = $val->nama_ikan;
					}
				}
				$arrIkan[] = $row2;
				$n++;
			}
			$row['produksi'] = $arrIkan;
			$row['ikan'] = $row3;
			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($tangkapan),
			"recordsFiltered" => count($tangkapan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById()
	{
		$id = $this->input->get('id_data_tangkapan');
		$this->db->select('*');
		$this->db->from('tb_data_tangkapan');
		$this->db->join('tb_kecamatan', 'tb_data_tangkapan.kd_kecamatan = tb_kecamatan.kd_kecamatan');
		$this->db->where('id_data_tangkapan', $id);
		$Data = $this->db->get()->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		// $fields = $this->tangkapan->ListColumnName();
		$post = $this->input->post();

		$data1 = [
			'jml_nelayan' => $post['perahu_motor'] + $post['tanpa_motor'] + $post['tanpa_perahu'],
			'perahu_motor_tempel' => $post['perahu_motor'],
			'perahu_tanpa_motor' => $post['tanpa_motor'],
			'nelayan_tanpa_perahu' => $post['tanpa_perahu'],
		];
		if ($id == null) {
			$data1['kd_kecamatan'] = $post['kec'];
			$data1['created'] = $post['thn'] . '-' . $post['bln'];
			$this->db->insert('tb_data_tangkapan', $data1);
			$id_data = $this->db->insert_id();
		} else {
			$this->db->where('id_data_tangkapan', $id);
			$this->db->update('tb_data_tangkapan', $data1);
			$id_data = $id;
		}
		$ikan = $this->db->get('tb_jenis_ikan')->result();
		$data = array();
		$n = 0;
		foreach ($ikan as $value) {
			$id_ikan = $value->id_jenis_ikan;
			if ($id_ikan != 6 && $id_ikan != 8 && $id_ikan != 23) {
				$row = array();
				$alat = $this->db->get('tb_data_alat_tangkap')->result();
				foreach ($alat as $key => $val) {
					if ($post[$id_ikan][$key] != '') {
						$row['id_data_tangkapan'] = $id_data;
						$row['id_alat_tangkap'] = $val->id_data_alat_tangkap;
						$row['id_jenis_ikan'] = $id_ikan;
						$row['produksi'] = $post[$id_ikan][$key];
						$row['id_data_tangkapan_transaksi'] = $post['trxikan'][$n];
						$data[] = $row;
					}
					$n++;
				}
			}
		}
		if ($data == null) {
			$data = 'Tidak ada Transaksi';
		} else {
			if ($id == null) {
				$this->db->insert_batch('tb_data_tangkapan_transaksi', $data);
			} else {
				foreach ($data as $tes) {
					$this->db->where('id_data_tangkapan_transaksi', $tes['id_data_tangkapan_transaksi']);
					$db = $this->db->get('tb_data_tangkapan_transaksi')->row();
					if ($db == null) {
						$this->db->insert('tb_data_tangkapan_transaksi', $tes);
					} else {
						$this->db->set($tes);
						$this->db->where('id_data_tangkapan_transaksi', $tes['id_data_tangkapan_transaksi']);
						$this->db->update('tb_data_tangkapan_transaksi');
					}
				}
				// 	// $this->db->update_batch('tb_data_tangkapan_transaksi', $data, 'id_data_tangkapan');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode(['data' => $data]));
	}
	// public function DeleteByAlat()
	// {
	// 	$this->db->where('id_alat_tangkap', $this->input->post('id'));
	// }
	public function Delete($data = null)
	{
		switch ($data) {
			case '1':
				$this->db->where('id_data_tangkapan', $this->input->post('id'));
				$result = $this->db->delete('tb_data_tangkapan_transaksi');
				break;

			default:
				if ($this->input->post('id')) {
					$table = array('tb_data_tangkapan', 'tb_data_tangkapan_transaksi');
					$this->db->where('id_data_tangkapan', $this->input->post('id'));
					$result = $this->db->delete($table);
				} else {
					$this->db->where('id_alat_tangkap', $this->input->post('id_alat_tangkap'));
					$result = $this->db->delete('tb_data_tangkapan_transaksi');
				}
				break;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function Check()
	{
		$this->db->where('created', $this->input->post('tahun') . '-' . $this->input->post('bulan'));
		$this->db->where('kd_kecamatan', $this->input->post('kec'));
		$IsDataExist = $this->db->get('tb_data_tangkapan');
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_data_tangkapan_transaksi', ['id_data_tangkapan' => $IsDataExist->row()->id_data_tangkapan]);
			if ($DataTrx->num_rows() > 0) {
				$Response = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Master dan Data Transaksi Ada, Silahkan Melakukan Perubahan Data',
					'data' => $IsDataExist->row()->id_data_tangkapan,
					'perahu_motor' => $IsDataExist->row()->perahu_motor_tempel,
					'tanpa_motor' => $IsDataExist->row()->perahu_tanpa_motor,
					'tanpa_perahu' => $IsDataExist->row()->nelayan_tanpa_perahu,
				];
			} else {
				$Response = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Data Master Ada, Silahkan Melakukan Penambahan Data',
					'data' => $IsDataExist->row()->id_data_tangkapan,
					'perahu_motor' => $IsDataExist->row()->perahu_motor_tempel,
					'tanpa_motor' => $IsDataExist->row()->perahu_tanpa_motor,
					'tanpa_perahu' => $IsDataExist->row()->nelayan_tanpa_perahu,
				];
			}
		} else {
			$Response = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Data Belum Ada, Silahkan Tambah Data',
				'data' => $IsDataExist->row()->id_data_tangkapan
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}
	public function DataTablesForm()
	{
		$id_tangkapan = $this->input->post('id_data_tangkapan');
		$ikan = $this->db->get('tb_jenis_ikan')->result();
		foreach ($ikan as $val) {
			$id = $val->id_jenis_ikan;
			if ($val->tangkapan == 1) {
				$row = array();
				$row[] = '<input type="hidden" name="id_jenis_ikan[]" value="' . $val->id_jenis_ikan . '" readonly class="form-control"/>';
				$row[] = $val->nama_ikan;
				$alat = $this->db->get('tb_data_alat_tangkap')->result();
				foreach ($alat as $v) {
					if ($id_tangkapan) {
						$this->db->where('id_data_tangkapan', $id_tangkapan);
						$attr = '';
					} else {
						$attr = 'readonly';
					}
					$prod = $this->db->get_where('tb_data_tangkapan_transaksi', ['id_alat_tangkap' => $v->id_data_alat_tangkap, 'id_jenis_ikan' => $val->id_jenis_ikan, 'id_data_tangkapan' => $id_tangkapan])->row();
					$hasil = ($prod->produksi == '' ? ' ' : $prod->produksi);
					$row[] = '<input type="number" step="1" min="0"  name="' . $val->id_jenis_ikan . '[]" value="' . $hasil . '" class="form-control " ' . $attr . '/> <input type="hidden" name="trxikan[]" value="' . $prod->id_tangkapan_transaksi . '"></input>';
				}
				$data[] = $row;
			}
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($ikan),
			"recordsFiltered" => count($ikan),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Produksi.php */
/* Location: ./application/modules/diskan/controllers/Produksi.php */