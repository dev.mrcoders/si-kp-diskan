<style>
    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }

    table.dataTable thead th {
        white-space: nowrap
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">

                <div class="card-body ">
                    <div class="row mb-4">
                        <div class="col-sm-3">
                            <h4 class="card-title">Data Kegiatan</h4>
                        </div>
                        <div class="col-sm-9 text-right">
                            <button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table nowrap" id="tb-data" width="100%">
                            <thead class="bg-info text-white">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Data Jenis kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="<?= base_url() ?>jenis-kegiatan/save" id="form-kegiatan">
                <input type="hidden" name="data_jenis" value="kegiatan">
                <div class="modal-body" id="fields-komoditi">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="komoditi" name="jenis[]" placeholder="Nama kegiatan" required>
                            </div>
                        </div>
                        <div class="col-sm-2" id="btn-row-add">
                            <div class="form-group">
                                <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-outline-danger waves-effect waves-light"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    const base_url = '<?= base_url() ?>';
    $(function() {
        Datatables();

        function Datatables() {
            $('#tb-data').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                },
                stateSave: true,
                destroy: true,
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": base_url + "jenis-kegiatan/tables",
                    "type": "POST",
                    "data": function(data) {

                    }
                }
            });
        }

        $('.add').on('click', function(event) {
            event.preventDefault();
            $('form#form-kegiatan').attr('action', base_url + 'jenis-kegiatan/save');
            $('form#form-kegiatan')[0].reset();
            $('#add-modal').modal({
                backdrop: 'static',
                keyboard: false
            })
        });

        $('tbody').on('click', '.edit', function(event) {
            event.preventDefault();
            let id = $(this).data('id');
            $.ajax({
                url: base_url + 'jenis-kegiatan/databyid/kegiatan',
                type: 'get',
                data: {
                    id: id
                },
                success: function(data) {
                    $('form#form-kegiatan').attr('action', base_url + 'jenis-kegiatan/save/' + id);
                    $('#komoditi').val(data.nama_kegiatan);
                    $('#btn-row-add').hide('fast');
                    $('#add-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                }
            });

        });

        $('tbody').on('click', '.delete', function(event) {
            event.preventDefault();
            let id = $(this).data('id');
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Data akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya Hapus!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url + 'jenis-kegiatan/delete/kegiatan',
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                            Datatables();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Data Berhasil Dihapus',
                                showConfirmButton: false,
                                timer: 1500
                            })

                        }
                    });

                }
            })

        });

        $('form#form-kegiatan').submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            let Form = $(this).serialize();
            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                data: Form,
                success: function(data, textStatus, xhr) {
                    if (xhr.status == 200) {
                        // console.log(data);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Data Berhasil Disimpan!',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#add-modal').modal('hide');
                        Datatables();
                    }
                },
                error: function(request, textStatus, errorThrown) {
                    console.log(request.responseText);
                }
            });
        });
    });

    var room = 1;

    function education_fields() {

        room++;
        var objTo = document.getElementById('fields-komoditi')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "row removeclass" + room);
        var rdiv = 'removeclass' + room;
        divtest.innerHTML = '<div class="col-sm-10"><div class="form-group"><input type="text" class="form-control" id="Schoolname" name="jenis[]" placeholder="Nama kegiatan" required></div></div><div class="col-sm-2"> <div class="form-group"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <i class="fa fa-minus"></i> </button> </div></div></div>';

        objTo.appendChild(divtest)
    }

    function remove_education_fields(rid) {
        $('.removeclass' + rid).remove();
    }
</script>