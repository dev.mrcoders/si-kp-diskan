-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 14 Jan 2023 pada 22.13
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_diskan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_alat_tangkap`
--

CREATE TABLE `tb_data_alat_tangkap` (
  `id_data_alat_tangkap` int(5) NOT NULL,
  `nama_alat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_data_alat_tangkap`
--

INSERT INTO `tb_data_alat_tangkap` (`id_data_alat_tangkap`, `nama_alat`) VALUES
(1, 'Jaring Tetap'),
(2, 'Jaring Hanyut'),
(3, 'Bubu'),
(4, 'Rawai'),
(5, 'Pancing'),
(6, 'Jala');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_budidaya`
--

CREATE TABLE `tb_data_budidaya` (
  `id_data_budidaya` int(15) NOT NULL,
  `created` varchar(15) NOT NULL,
  `kd_kecamatan` int(45) NOT NULL,
  `jumlah` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_budidaya`
--

INSERT INTO `tb_data_budidaya` (`id_data_budidaya`, `created`, `kd_kecamatan`, `jumlah`) VALUES
(6, '2022-01', 1406011, '12'),
(8, '2022-01', 1406013, '20000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_budidaya_transaksi`
--

CREATE TABLE `tb_data_budidaya_transaksi` (
  `id_data_budidaya_transaksi` int(15) NOT NULL,
  `id_data_budidaya` varchar(15) NOT NULL,
  `id_jenis_ikan` varchar(15) NOT NULL,
  `jumlah` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_budidaya_transaksi`
--

INSERT INTO `tb_data_budidaya_transaksi` (`id_data_budidaya_transaksi`, `id_data_budidaya`, `id_jenis_ikan`, `jumlah`) VALUES
(9, '6', '2', '2'),
(10, '6', '3', '3'),
(11, '6', '4', '5'),
(12, '6', '5', '2'),
(14, '8', '3', '20000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_kegiatan`
--

CREATE TABLE `tb_data_kegiatan` (
  `id_data_kegiatan` int(5) NOT NULL,
  `nama_kegiatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_data_kegiatan`
--

INSERT INTO `tb_data_kegiatan` (`id_data_kegiatan`, `nama_kegiatan`) VALUES
(1, 'Pengolahan Lainnya'),
(2, 'Penggaraman/Pengeringan'),
(3, 'Pengasapan/Pemanggangan'),
(4, 'Penanganan Produk Segar/Dingin'),
(8, 'Pelumatan Daging/Surimi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_pembenihan`
--

CREATE TABLE `tb_data_pembenihan` (
  `id_data_pembenihan` int(15) NOT NULL,
  `kd_kecamatan` varchar(15) NOT NULL,
  `created` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_pembenihan`
--

INSERT INTO `tb_data_pembenihan` (`id_data_pembenihan`, `kd_kecamatan`, `created`) VALUES
(1, '1406010', '2022-01'),
(2, '1406011', '2021-03'),
(3, '1406010', '2023-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_pembenihan_transaksi`
--

CREATE TABLE `tb_data_pembenihan_transaksi` (
  `id_data_pembenihan_transaksi` int(15) NOT NULL,
  `id_data_pembenihan` varchar(15) NOT NULL,
  `id_jenis_ikan` varchar(15) NOT NULL,
  `upr_aktif` varchar(15) NOT NULL,
  `periode` varchar(15) NOT NULL,
  `jumlah_bibit` varchar(15) NOT NULL,
  `kapasitas_min` varchar(15) NOT NULL,
  `kapasitas_max` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_pembenihan_transaksi`
--

INSERT INTO `tb_data_pembenihan_transaksi` (`id_data_pembenihan_transaksi`, `id_data_pembenihan`, `id_jenis_ikan`, `upr_aktif`, `periode`, `jumlah_bibit`, `kapasitas_min`, `kapasitas_max`) VALUES
(2, '2', '1', '2', '2', '3', '12', '12'),
(3, '3', '2', '2', '2', '41', '31', '31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_pengolahan`
--

CREATE TABLE `tb_data_pengolahan` (
  `id_data_pengolahan` int(15) NOT NULL,
  `kd_kecamatan` varchar(15) NOT NULL,
  `jumlah_upi` varchar(15) NOT NULL,
  `created` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_pengolahan`
--

INSERT INTO `tb_data_pengolahan` (`id_data_pengolahan`, `kd_kecamatan`, `jumlah_upi`, `created`) VALUES
(23, '1406012', '5', '2021-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_pengolahan_transaksi`
--

CREATE TABLE `tb_data_pengolahan_transaksi` (
  `id_data_pengolahan_transaksi` int(15) NOT NULL,
  `id_data_pengolahan` varchar(15) NOT NULL,
  `id_data_kegiatan` varchar(15) NOT NULL,
  `nama_produk_olahan` varchar(15) NOT NULL,
  `volume_perbulan` varchar(15) NOT NULL,
  `siklus_produksi` varchar(15) NOT NULL,
  `produksi_tahunan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_pengolahan_transaksi`
--

INSERT INTO `tb_data_pengolahan_transaksi` (`id_data_pengolahan_transaksi`, `id_data_pengolahan`, `id_data_kegiatan`, `nama_produk_olahan`, `volume_perbulan`, `siklus_produksi`, `produksi_tahunan`) VALUES
(28, '23', '1', '2', '2', '3', '24'),
(29, '23', '1', '10', '2', '3', '24'),
(30, '23', '3', '3', '5', '3', '60'),
(31, '23', '3', '8', '5', '3', '60'),
(32, '23', '4', '6', '5', '1', '60'),
(44, '23', '4', '6', '5', '1', '60');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_tangkapan`
--

CREATE TABLE `tb_data_tangkapan` (
  `id_data_tangkapan` int(15) NOT NULL,
  `kd_kecamatan` varchar(15) NOT NULL,
  `jml_nelayan` varchar(15) NOT NULL,
  `perahu_motor_tempel` varchar(15) NOT NULL,
  `perahu_tanpa_motor` varchar(15) NOT NULL,
  `nelayan_tanpa_perahu` varchar(15) DEFAULT NULL,
  `created` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_tangkapan`
--

INSERT INTO `tb_data_tangkapan` (`id_data_tangkapan`, `kd_kecamatan`, `jml_nelayan`, `perahu_motor_tempel`, `perahu_tanpa_motor`, `nelayan_tanpa_perahu`, `created`) VALUES
(5, '1406010', '5', '1', '2', '2', '2021-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_tangkapan_transaksi`
--

CREATE TABLE `tb_data_tangkapan_transaksi` (
  `id_data_tangkapan_transaksi` int(15) NOT NULL,
  `id_data_tangkapan` varchar(15) NOT NULL,
  `id_alat_tangkap` varchar(15) NOT NULL,
  `id_jenis_ikan` varchar(15) NOT NULL,
  `produksi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_tangkapan_transaksi`
--

INSERT INTO `tb_data_tangkapan_transaksi` (`id_data_tangkapan_transaksi`, `id_data_tangkapan`, `id_alat_tangkap`, `id_jenis_ikan`, `produksi`) VALUES
(2, '5', '2', '2', '2'),
(3, '5', '2', '2', '2'),
(4, '5', '1', '3', '3'),
(5, '5', '2', '2', '2'),
(6, '5', '1', '3', '3'),
(7, '5', '3', '4', '5');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_ikan`
--

CREATE TABLE `tb_jenis_ikan` (
  `id_jenis_ikan` int(15) NOT NULL,
  `nama_ikan` varchar(75) NOT NULL,
  `budidaya` tinyint(1) DEFAULT '0',
  `tangkapan` tinyint(1) DEFAULT '0',
  `benih` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_ikan`
--

INSERT INTO `tb_jenis_ikan` (`id_jenis_ikan`, `nama_ikan`, `budidaya`, `tangkapan`, `benih`) VALUES
(2, 'Jelawat', 1, 1, 1),
(3, 'Hampala', 1, 1, 1),
(4, 'Tambakan', 1, 1, 1),
(5, 'Gabus', 1, 1, 1),
(6, 'Baung', 1, 1, 1),
(7, 'Tapah', 0, 1, 0),
(8, 'Motan', 0, 1, 0),
(9, 'Toman', 0, 1, 0),
(10, 'Kopiek', 0, 1, 0),
(11, 'Pantau', 0, 1, 0),
(12, 'Lele', 0, 1, 1),
(13, 'Mas', 1, 1, 1),
(14, 'Bawal', 1, 0, 1),
(15, 'Kelemak', 1, NULL, NULL),
(16, 'Patin', 1, 1, 1),
(17, 'Tawes', 0, 1, 0),
(18, 'Nila', 0, 1, 0),
(19, 'Gurami', 0, 1, 1),
(20, 'Koi', 0, 0, 1),
(21, 'Komet', 0, 0, 1),
(22, 'Guppy', 0, 0, 1),
(23, 'Mas Koki', 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_olahan`
--

CREATE TABLE `tb_jenis_olahan` (
  `id_jenis_olahan` int(5) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_jenis_olahan`
--

INSERT INTO `tb_jenis_olahan` (`id_jenis_olahan`, `nama_jenis`) VALUES
(1, 'Amplang'),
(2, 'Bakso Patin'),
(3, 'Bakso Patin'),
(4, 'Fillet Tuna'),
(5, 'Ikan Jelawat Asin'),
(6, 'Ikan Segar Jelawat'),
(7, 'Ikan Segar Lele'),
(8, 'Ikan Segar Mas'),
(9, 'Ikan Segar Mas'),
(10, 'Ikan Segar Nila'),
(11, 'Ikan Segar Patin'),
(12, 'Kerupuk Ikan'),
(13, 'Kerupuk Ikan Teri'),
(14, 'Kerupuk Udang'),
(15, 'Mie Ikan'),
(16, 'Otak Otak Ikan'),
(17, 'Salai Lele'),
(18, 'Salai Patim'),
(19, 'Salai Patin'),
(20, 'Salai Paton'),
(21, 'Sarden'),
(22, 'Nugget Patin'),
(23, 'Bakso Tuna ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kecamatan`
--

CREATE TABLE `tb_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `kd_kecamatan` varchar(25) DEFAULT NULL,
  `nama_kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kecamatan`
--

INSERT INTO `tb_kecamatan` (`id_kecamatan`, `kd_kecamatan`, `nama_kecamatan`) VALUES
(1, '1406010', 'Kampar Kiri'),
(2, '1406011', 'Kampar Kiri Hulu'),
(3, '1406012', 'Kampar Kiri Hilir'),
(4, '1406013', 'Gunung Sahilan'),
(5, '1406014', 'Kampar Kiri Tengah'),
(6, '1406020', 'Xiii Koto Kampar'),
(7, '1406021', 'Koto Kampar Hulu'),
(8, '1406030', 'Kuok'),
(9, '1406031', 'Salo'),
(10, '1406040', 'Tapung'),
(11, '1406041', 'Tapung Hulu'),
(12, '1406042', 'Tapung Hilir'),
(13, '1406050', 'Bangkinang Kota'),
(14, '1406051', 'Bangkinang'),
(15, '1406060', 'Kampar'),
(16, '1406061', 'Kampa'),
(17, '1406062', 'Rumbio Jaya'),
(18, '1406063', 'Kampar Utara'),
(19, '1406070', 'Tambang'),
(20, '1406080', 'Siak Hulu'),
(21, '1406081', 'Perhentian Raja'),
(22, '0', 'No Opsi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kelurahan`
--

CREATE TABLE `tb_kelurahan` (
  `id_kelurahan` int(11) NOT NULL,
  `kd_kecamatan` varchar(20) DEFAULT NULL,
  `kd_kelurahan` varchar(25) NOT NULL,
  `nama_kelurahan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kelurahan`
--

INSERT INTO `tb_kelurahan` (`id_kelurahan`, `kd_kecamatan`, `kd_kelurahan`, `nama_kelurahan`) VALUES
(1, '1406010', '1406010006', 'Domo'),
(2, '1406010', '1406010014', 'Muara Selaya'),
(3, '1406010', '1406010015', 'Iv Koto Setingkai'),
(4, '1406010', '1406010016', 'Padang Sawah'),
(5, '1406010', '1406010017', 'Kuntu'),
(6, '1406010', '1406010018', 'Teluk Paman'),
(7, '1406010', '1406010019', 'Lipat Kain'),
(8, '1406010', '1406010020', 'Sungai Geringging'),
(9, '1406010', '1406010021', 'Sungai Paku'),
(10, '1406010', '1406010034', 'Sungai Rambai'),
(11, '1406010', '1406010035', 'Sungai Raja'),
(12, '1406010', '1406010036', 'Tanjung Harapan'),
(13, '1406010', '1406010037', 'Sungai Sarik'),
(14, '1406010', '1406010040', 'Lipat Kain Utara'),
(15, '1406010', '1406010041', 'Lipat Kain Selatan'),
(16, '1406010', '1406010042', 'Kuntu Darussalam'),
(17, '1406010', '1406010043', 'Tanjung Mas'),
(18, '1406010', '1406010044', 'Sungai Liti'),
(19, '1406010', '1406010045', 'Teluk Paman Timur'),
(20, '1406010', '1406010046', 'Sungai Harapan'),
(21, '1406011', '1406011001', 'Aur Kuning'),
(22, '1406011', '1406011002', 'Tanjung Beringin'),
(23, '1406011', '1406011003', 'Batu Sanggan'),
(24, '1406011', '1406011004', 'Tanjung Belit'),
(25, '1406011', '1406011005', 'Gema'),
(26, '1406011', '1406011006', 'Tanjung Belit Selatan'),
(27, '1406011', '1406011007', 'Kota Lama'),
(28, '1406011', '1406011008', 'Ludai'),
(29, '1406011', '1406011009', 'Pangkalan Kapas'),
(30, '1406011', '1406011010', 'Kebun Tinggi'),
(31, '1406011', '1406011011', 'Batu Sasak'),
(32, '1406011', '1406011012', 'Tanjung Karang'),
(33, '1406011', '1406011013', 'Gajah Bertalut'),
(34, '1406011', '1406011014', 'Pangkalan Serai'),
(35, '1406011', '1406011015', 'Danau Sontul'),
(36, '1406011', '1406011016', 'Deras Tajak'),
(37, '1406011', '1406011017', 'Terusan'),
(38, '1406011', '1406011018', 'Sungai Santi'),
(39, '1406011', '1406011019', 'Tanjung Permai'),
(40, '1406011', '1406011020', 'Dua Sepakat'),
(41, '1406011', '1406011021', 'Subayang Jaya'),
(42, '1406011', '1406011022', 'Bukit Betung'),
(43, '1406011', '1406011023', 'Lubuk Bigau'),
(44, '1406011', '1406011024', 'Muara Bio'),
(45, '1406012', '1406012001', 'Sungai Simpang Dua'),
(46, '1406012', '1406012002', 'Sungai Pagar'),
(47, '1406012', '1406012003', 'Mentulik'),
(48, '1406012', '1406012004', 'Bangun Sari'),
(49, '1406012', '1406012005', 'Sungai Petai'),
(50, '1406012', '1406012006', 'Rantau Kasih'),
(51, '1406012', '1406012007', 'Sungai Bunga'),
(52, '1406012', '1406012008', 'Gading Permai'),
(53, '1406013', '1406013001', 'Kebun Durian'),
(54, '1406013', '1406013002', 'Subarak'),
(55, '1406013', '1406013003', 'Gunung Sahilan'),
(56, '1406013', '1406013004', 'Suka Makmur'),
(57, '1406013', '1406013005', 'Gunung Sari'),
(58, '1406013', '1406013006', 'Makmur Sejahtera'),
(59, '1406013', '1406013007', 'Sungai Lipai'),
(60, '1406013', '1406013008', 'Sahilan Darussalam'),
(61, '1406013', '1406013009', 'Gunung Mulya'),
(62, '1406014', '1406014001', 'Penghidupan'),
(63, '1406014', '1406014002', 'Simalinyang'),
(64, '1406014', '1406014003', 'Mayang Pongkai'),
(65, '1406014', '1406014004', 'Lubuk Sakai'),
(66, '1406014', '1406014005', 'Bina Baru'),
(67, '1406014', '1406014006', 'Hidup Baru'),
(68, '1406014', '1406014007', 'Karya Bakti'),
(69, '1406014', '1406014008', 'Koto Damai'),
(70, '1406014', '1406014009', 'Utama Karya'),
(71, '1406014', '1406014010', 'Bukit Sakai'),
(72, '1406014', '1406014011', 'Mekar Jaya'),
(73, '1406020', '1406020001', 'Balung'),
(74, '1406020', '1406020002', 'Pulau Gadang'),
(75, '1406020', '1406020003', 'Tanjung Alai'),
(76, '1406020', '1406020004', 'Batu Bersurat'),
(77, '1406020', '1406020005', 'Koto Tuo'),
(78, '1406020', '1406020006', 'Muara Takus'),
(79, '1406020', '1406020007', 'Gunung Bungsu'),
(80, '1406020', '1406020013', 'Koto Mesjid'),
(81, '1406020', '1406020014', 'Lubuk Agung'),
(82, '1406020', '1406020015', 'Ranah Sungkai'),
(83, '1406020', '1406020016', 'Binamang'),
(84, '1406020', '1406020017', 'Pongkai Istiqamah'),
(85, '1406020', '1406020024', 'Koto Tuo Barat'),
(86, '1406021', '1406021001', 'Tanjung'),
(87, '1406021', '1406021002', 'Tabing'),
(88, '1406021', '1406021003', 'Pongkai'),
(89, '1406021', '1406021004', 'Gunung Malelo'),
(90, '1406021', '1406021005', 'Sibiruang'),
(91, '1406021', '1406021006', 'Bandur Picak'),
(92, '1406030', '1406030002', 'Kuok'),
(93, '1406030', '1406030003', 'Merangin'),
(94, '1406030', '1406030004', 'Empat Balai'),
(95, '1406030', '1406030005', 'Pulau Jambu'),
(96, '1406030', '1406030008', 'Silam'),
(97, '1406030', '1406030009', 'Bukit Melintang'),
(98, '1406030', '1406030010', 'Lereng'),
(99, '1406030', '1406030011', 'Pulau Terap'),
(100, '1406030', '1406030012', 'Batu Langka Kecil'),
(101, '1406031', '1406031001', 'Siabu'),
(102, '1406031', '1406031002', 'Ganting'),
(103, '1406031', '1406031003', 'Sipungguk'),
(104, '1406031', '1406031004', 'Ganting Damai'),
(105, '1406031', '1406031005', 'Salo'),
(106, '1406031', '1406031006', 'Salo Timur'),
(107, '1406040', '1406040002', 'Petapahan'),
(108, '1406040', '1406040003', 'Sei Lembu Makmur'),
(109, '1406040', '1406040004', 'Muara Mahat Baru'),
(110, '1406040', '1406040005', 'Kinantan'),
(111, '1406040', '1406040006', 'Sibuak'),
(112, '1406040', '1406040007', 'Pantai Cermin'),
(113, '1406040', '1406040008', 'Sei Putih'),
(114, '1406040', '1406040009', 'Pagaruyung'),
(115, '1406040', '1406040010', 'Air Terbit'),
(116, '1406040', '1406040011', 'Pancuran Gading'),
(117, '1406040', '1406040012', 'Sari Galuh'),
(118, '1406040', '1406040013', 'Tri Manunggal'),
(119, '1406040', '1406040014', 'Mukti Sari'),
(120, '1406040', '1406040015', 'Indrapuri'),
(121, '1406040', '1406040016', 'Gading Sari'),
(122, '1406040', '1406040017', 'Tanjung Sawit'),
(123, '1406040', '1406040018', 'Sumber Makmur'),
(124, '1406040', '1406040019', 'Petapahan Jaya'),
(125, '1406040', '1406040030', 'Pelambaian'),
(126, '1406040', '1406040031', 'Indra Sakti'),
(127, '1406040', '1406040032', 'Sungai Agung'),
(128, '1406040', '1406040033', 'Karya Indah'),
(129, '1406040', '1406040034', 'Kijang Rejo'),
(130, '1406040', '1406040035', 'Bencah Kelubi'),
(131, '1406040', '1406040036', 'Batu Gajah'),
(132, '1406041', '1406041001', 'Kasikan'),
(133, '1406041', '1406041002', 'Rimba Beringin'),
(134, '1406041', '1406041003', 'Senama Nenek'),
(135, '1406041', '1406041004', 'Bukit Kemuning'),
(136, '1406041', '1406041005', 'Danau Lancang'),
(137, '1406041', '1406041006', 'Suka Ramai'),
(138, '1406041', '1406041007', 'Kusau Makmur'),
(139, '1406041', '1406041008', 'Sumber Sari'),
(140, '1406041', '1406041014', 'Talang Danto'),
(141, '1406041', '1406041015', 'Muara Intan'),
(142, '1406041', '1406041016', 'Intan Jaya'),
(143, '1406041', '1406041017', 'Tanah Datar'),
(144, '1406041', '1406041018', 'Rimba Makmur'),
(145, '1406041', '1406041019', 'Rimba Jaya'),
(146, '1406042', '1406042001', 'Sekijang'),
(147, '1406042', '1406042002', 'Tebing Lestari'),
(148, '1406042', '1406042003', 'Kijang Jaya'),
(149, '1406042', '1406042004', 'Tanah Tinggi'),
(150, '1406042', '1406042005', 'Tapung Makmur'),
(151, '1406042', '1406042006', 'Tapung Lestari'),
(152, '1406042', '1406042007', 'Kota Garo'),
(153, '1406042', '1406042008', 'Suka Maju'),
(154, '1406042', '1406042009', 'Kota Baru'),
(155, '1406042', '1406042010', 'Koto Bangun'),
(156, '1406042', '1406042011', 'Cinta Damai'),
(157, '1406042', '1406042012', 'Beringin Lestari'),
(158, '1406042', '1406042013', 'Tandan Sari'),
(159, '1406042', '1406042015', 'Kijang Makmur'),
(160, '1406042', '1406042016', 'Koto Aman'),
(161, '1406042', '1406042017', 'Gerbang Sari'),
(162, '1406050', '1406050002', 'Langgini'),
(163, '1406050', '1406050003', 'Bangkinang'),
(164, '1406050', '1406050013', 'Kumantan'),
(165, '1406050', '1406050014', 'Ridan Permai'),
(166, '1406051', '1406051001', 'Pulau Lawas'),
(167, '1406051', '1406051002', 'Muara Uwai'),
(168, '1406051', '1406051003', 'Pulau'),
(169, '1406051', '1406051004', 'Pasir Sialang'),
(170, '1406051', '1406051005', 'Bukit Sembilan'),
(171, '1406051', '1406051006', 'Laboi Jaya'),
(172, '1406051', '1406051007', 'Suka Mulya'),
(173, '1406051', '1406051008', 'Bukit Payung'),
(174, '1406051', '1406051009', 'Binuang'),
(175, '1406060', '1406060001', 'Batu Belah'),
(176, '1406060', '1406060002', 'Tanjung Berulak'),
(177, '1406060', '1406060003', 'Air Tiris'),
(178, '1406060', '1406060004', 'Ranah'),
(179, '1406060', '1406060005', 'Penyasawan'),
(180, '1406060', '1406060006', 'Rumbio'),
(181, '1406060', '1406060007', 'Padang Mutung'),
(182, '1406060', '1406060025', 'Simpang Kubu'),
(183, '1406060', '1406060026', 'Tanjung Rambutan'),
(184, '1406060', '1406060028', 'Pulau Jambu'),
(185, '1406060', '1406060029', 'Limau Manis'),
(186, '1406060', '1406060030', 'Naumbai'),
(187, '1406060', '1406060031', 'Ranah Singkuang'),
(188, '1406060', '1406060032', 'Pulau Tinggi'),
(189, '1406060', '1406060033', 'Koto Tibun'),
(190, '1406060', '1406060034', 'Bukit Ranah'),
(191, '1406060', '1406060035', 'Ranah Baru'),
(192, '1406060', '1406060036', 'Pulau Sarak'),
(193, '1406061', '1406061001', 'Pulau Rambai'),
(194, '1406061', '1406061002', 'Kampar'),
(195, '1406061', '1406061003', 'Koto Perambahan'),
(196, '1406061', '1406061004', 'Pulau Birandang'),
(197, '1406061', '1406061005', 'Sungai Putih'),
(198, '1406061', '1406061006', 'Deli Makmur'),
(199, '1406061', '1406061007', 'Sungai Tarap'),
(200, '1406061', '1406061008', 'Tanjung Bungo'),
(201, '1406061', '1406061009', 'Sawah Baru'),
(202, '1406062', '1406062001', 'Alam Panjang'),
(203, '1406062', '1406062002', 'Pulau Payung'),
(204, '1406062', '1406062003', 'Teratak'),
(205, '1406062', '1406062004', 'Bukit Kratai'),
(206, '1406062', '1406062005', 'Batang Batindih'),
(207, '1406062', '1406062006', 'Tambusai'),
(208, '1406062', '1406062007', 'Simpang Petai'),
(209, '1406063', '1406063001', 'Kampung Panjang'),
(210, '1406063', '1406063002', 'Sawah'),
(211, '1406063', '1406063003', 'Kayu Aro'),
(212, '1406063', '1406063004', 'Muara Jalai'),
(213, '1406063', '1406063005', 'Sungai Tonang'),
(214, '1406063', '1406063006', 'Sungai Jalau'),
(215, '1406063', '1406063007', 'Sendayan'),
(216, '1406063', '1406063008', 'Naga Beralih'),
(217, '1406070', '1406070001', 'Kuapan'),
(218, '1406070', '1406070002', 'Aur Sati'),
(219, '1406070', '1406070003', 'Tambang'),
(220, '1406070', '1406070004', 'Padang Luas'),
(221, '1406070', '1406070005', 'Gobah'),
(222, '1406070', '1406070006', 'Terantang'),
(223, '1406070', '1406070007', 'Rimba Panjang'),
(224, '1406070', '1406070008', 'Kualu'),
(225, '1406070', '1406070009', 'Teluk Kenidai'),
(226, '1406070', '1406070010', 'Parit Baru'),
(227, '1406070', '1406070011', 'Kemang Indah'),
(228, '1406070', '1406070012', 'Sungai Pinang'),
(229, '1406070', '1406070013', 'Kualu Nenas'),
(230, '1406070', '1406070014', 'Tarai Bangun'),
(231, '1406070', '1406070015', 'Palung Raya'),
(232, '1406070', '1406070016', 'Pulau Permai'),
(233, '1406070', '1406070017', 'Balam Jaya'),
(234, '1406080', '1406080004', 'Buluh Nipis'),
(235, '1406080', '1406080005', 'Pangkalan Baru'),
(236, '1406080', '1406080006', 'Buluh Cina'),
(237, '1406080', '1406080007', 'Lubuk Siam'),
(238, '1406080', '1406080009', 'Teratak Buluh'),
(239, '1406080', '1406080010', 'Desa Baru'),
(240, '1406080', '1406080011', 'Tanah Merah'),
(241, '1406080', '1406080012', 'Pandau Jaya'),
(242, '1406080', '1406080013', 'Pangkalan Serik'),
(243, '1406080', '1406080014', 'Kepau Jaya'),
(244, '1406080', '1406080015', 'Tanjung Balam'),
(245, '1406080', '1406080016', 'Kubang Jaya'),
(246, '1406081', '1406081001', 'Pantai Raja'),
(247, '1406081', '1406081002', 'Sialang Kubang'),
(248, '1406081', '1406081003', 'Hang Tuah'),
(249, '1406081', '1406081008', 'Kampung Pinang'),
(250, '1406081', '1406081009', 'Lubuk Sakat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` varchar(10) NOT NULL,
  `nama_role` varchar(50) NOT NULL,
  `rec_stat` char(1) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL,
  `user_created` varchar(20) NOT NULL,
  `user_updated` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `nama_role`, `rec_stat`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
('R1', 'Administrator', 'A', '2020-07-17 02:52:08', '2021-01-28 10:42:18', 'administrator', 'administrator'),
('R2', 'Staff Dinas', 'A', '2020-07-17 02:52:08', '2022-05-16 16:09:10', 'administrator', 'administrator'),
('R3', 'Koordinator', 'A', '2021-01-20 04:08:37', '2022-05-16 16:12:07', 'administrator', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_upr_tersedia`
--

CREATE TABLE `tb_upr_tersedia` (
  `id_upr_tersedia` int(5) NOT NULL,
  `kd_kecamatan` varchar(30) NOT NULL,
  `jumlah_upr` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_upr_tersedia`
--

INSERT INTO `tb_upr_tersedia` (`id_upr_tersedia`, `kd_kecamatan`, `jumlah_upr`) VALUES
(6, '1406010', '21'),
(7, '1406012', '13'),
(8, '1406014', '15'),
(9, '1406011', '30'),
(10, '1406013', '12'),
(11, '1406081', '15'),
(12, '1406080', '10'),
(13, '1406070', '15'),
(14, '1406063', '2'),
(15, '1406062', '7'),
(16, '1406061', '9'),
(17, '1406060', '23'),
(18, '1406051', '11'),
(19, '1406050', '9'),
(20, '1406042', '3'),
(21, '1406041', '6'),
(22, '1406040', '12'),
(23, '1406031', '4'),
(24, '1406030', '20'),
(25, '1406021', '12'),
(26, '1406020', '10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `kd_kecamatan` varchar(25) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(20) NOT NULL,
  `jabatan` varchar(25) NOT NULL,
  `is_user_login` char(1) DEFAULT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`kd_kecamatan`, `nip`, `password`, `nama`, `email`, `telepon`, `jabatan`, `is_user_login`, `tgl_created`, `tgl_updated`) VALUES
('0', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 'Adminstrator a', 'admin@mail.com', '090909', 'Admin Pusat', 'Y', '2023-01-13 17:33:39', '2023-01-13 17:33:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user_role`
--

CREATE TABLE `tb_user_role` (
  `id_user_role` int(11) NOT NULL,
  `kd_kecamatan` varchar(25) NOT NULL,
  `nipp` varchar(20) NOT NULL,
  `id_role` varchar(10) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user_role`
--

INSERT INTO `tb_user_role` (`id_user_role`, `kd_kecamatan`, `nipp`, `id_role`, `tgl_created`, `tgl_updated`) VALUES
(3, '0', 'Administrator', 'R1', '2022-12-02 19:49:51', '2022-12-02 19:49:51'),
(4, '1406010', 'penyuluh-1', 'R3', '2022-12-02 19:49:51', '2022-12-02 19:49:51');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_user`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_user` (
`id_user_role` int(11)
,`id_role` varchar(10)
,`nama_role` varchar(50)
,`nama_kecamatan` varchar(50)
,`kd_kecamatan` varchar(25)
,`nip` varchar(20)
,`password` varchar(255)
,`nama` varchar(100)
,`email` varchar(255)
,`phone` varchar(20)
,`jabatan` varchar(25)
,`is_user_login` char(1)
,`tgl_created` datetime
,`tgl_updated` datetime
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user`  AS SELECT `a`.`id_user_role` AS `id_user_role`, `c`.`id_role` AS `id_role`, `c`.`nama_role` AS `nama_role`, `d`.`nama_kecamatan` AS `nama_kecamatan`, `b`.`kd_kecamatan` AS `kd_kecamatan`, `b`.`nip` AS `nip`, `b`.`password` AS `password`, `b`.`nama` AS `nama`, `b`.`email` AS `email`, `b`.`telepon` AS `phone`, `b`.`jabatan` AS `jabatan`, `b`.`is_user_login` AS `is_user_login`, `b`.`tgl_created` AS `tgl_created`, `b`.`tgl_updated` AS `tgl_updated` FROM (((`tb_user_role` `a` join `tb_user` `b` on((`a`.`nipp` = `b`.`nip`))) join `tb_role` `c` on((`c`.`id_role` = `a`.`id_role`))) join `tb_kecamatan` `d` on((`a`.`kd_kecamatan` = `d`.`kd_kecamatan`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_data_alat_tangkap`
--
ALTER TABLE `tb_data_alat_tangkap`
  ADD PRIMARY KEY (`id_data_alat_tangkap`);

--
-- Indeks untuk tabel `tb_data_budidaya`
--
ALTER TABLE `tb_data_budidaya`
  ADD PRIMARY KEY (`id_data_budidaya`);

--
-- Indeks untuk tabel `tb_data_budidaya_transaksi`
--
ALTER TABLE `tb_data_budidaya_transaksi`
  ADD PRIMARY KEY (`id_data_budidaya_transaksi`);

--
-- Indeks untuk tabel `tb_data_kegiatan`
--
ALTER TABLE `tb_data_kegiatan`
  ADD PRIMARY KEY (`id_data_kegiatan`);

--
-- Indeks untuk tabel `tb_data_pembenihan`
--
ALTER TABLE `tb_data_pembenihan`
  ADD PRIMARY KEY (`id_data_pembenihan`);

--
-- Indeks untuk tabel `tb_data_pembenihan_transaksi`
--
ALTER TABLE `tb_data_pembenihan_transaksi`
  ADD PRIMARY KEY (`id_data_pembenihan_transaksi`);

--
-- Indeks untuk tabel `tb_data_pengolahan`
--
ALTER TABLE `tb_data_pengolahan`
  ADD PRIMARY KEY (`id_data_pengolahan`);

--
-- Indeks untuk tabel `tb_data_pengolahan_transaksi`
--
ALTER TABLE `tb_data_pengolahan_transaksi`
  ADD PRIMARY KEY (`id_data_pengolahan_transaksi`);

--
-- Indeks untuk tabel `tb_data_tangkapan`
--
ALTER TABLE `tb_data_tangkapan`
  ADD PRIMARY KEY (`id_data_tangkapan`);

--
-- Indeks untuk tabel `tb_data_tangkapan_transaksi`
--
ALTER TABLE `tb_data_tangkapan_transaksi`
  ADD PRIMARY KEY (`id_data_tangkapan_transaksi`);

--
-- Indeks untuk tabel `tb_jenis_ikan`
--
ALTER TABLE `tb_jenis_ikan`
  ADD PRIMARY KEY (`id_jenis_ikan`);

--
-- Indeks untuk tabel `tb_jenis_olahan`
--
ALTER TABLE `tb_jenis_olahan`
  ADD PRIMARY KEY (`id_jenis_olahan`);

--
-- Indeks untuk tabel `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indeks untuk tabel `tb_kelurahan`
--
ALTER TABLE `tb_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indeks untuk tabel `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `tb_user_role`
--
ALTER TABLE `tb_user_role`
  ADD PRIMARY KEY (`id_user_role`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_kelurahan`
--
ALTER TABLE `tb_kelurahan`
  MODIFY `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT untuk tabel `tb_user_role`
--
ALTER TABLE `tb_user_role`
  MODIFY `id_user_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
