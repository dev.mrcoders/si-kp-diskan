<?php $url = base_url() . 'assets/themav2/' ?>
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="card border-bottom border-info">
                <div class="card-body">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h2><?= $kecamatan ?></h2>
                            <h6 class="text-info">Kecamatan</h6>
                        </div>
                        <div class="ml-auto">
                            <span class="text-info display-6"><i class="ti-notepad"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card border-bottom border-cyan">
                <div class="card-body">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h2><?= $kelurahan ?></h2>
                            <h6 class="text-cyan">Kelurahan</h6>
                        </div>
                        <div class="ml-auto">
                            <span class="text-cyan display-6"><i class="ti-notepad"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card border-bottom border-success">
                <div class="card-body">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h2><?= $ikan ?></h2>
                            <h6 class="text-success">Jenis Ikan</h6>
                        </div>
                        <div class="ml-auto">
                            <span class="text-success display-6"><i class="fas fa-chart-area"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="card border-bottom border-orange">
                <div class="card-body">
                    <div class="d-flex no-block align-items-center">
                        <div>
                            <h2>100</h2>
                            <h6 class="text-orange">Komoditi Produksi</h6>
                        </div>
                        <div class="ml-auto">
                            <span class="text-orange display-6"><i class="fas fa-chart-line"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= base_url() ?>assets/themav2/assets/images/users/5.jpg" width="150" class="rounded-circle" alt="user" />
                        <h4 class="m-t-20 m-b-0"><?= $this->session->userdata('data')->nama; ?></h4>
                        <a href="mailto:danielkristeen@gmail.com"><?= $this->session->userdata('data')->email; ?></a>
                    </div>
                    <div class="badge badge-pill badge-light font-16">Role : <?= $this->session->userdata('data')->nama_role; ?></div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="card bg-dark">
                <img src="<?=base_url()?>assets/bg-1.jpeg" alt="" class="img-responsive img-fluid w-100">
                <div class="card-img-overlay">
                    <h2 class="font-weight-bold text-uppercase text-dark text-center">Selamat datang di website Dinas Perikanan Kabupaten Kampar</h2>
                </div>
            </div>
        </div>
    </div>
</div>